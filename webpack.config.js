module.exports = {
    context: __dirname,

    output: {
        filename: "bundle.js",
        path: __dirname
    },

    module: {
        loaders: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loaders: ["babel-loader"]
            },
            {
                test: /\.html$/,
                loader: "file?name=[name].[ext]",
            }
        ]
    },

    entry: {
        javascript: "./index.js",
        html: "./index.html"
    }
}
