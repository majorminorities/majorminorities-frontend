var assert = require('assert');
describe('Array', function() {
  describe('#indexOf()', function() {
    it('should return -1 when the value is not present', function() {
      assert.equal([1,2,3].indexOf(4), -1);
    });
  });
});

var request = require('request');
var expect = require('expect');
var util = require('util');
var baseUrl = "http://api.majorminorities.me"



describe('Accessing Minorities API', function() {
   it('should return the first minority', function(done) {
    request.get({ url: baseUrl + '/minority'},
    	function(error, response, body) {
    			var bodyObj = JSON.parse(body);
    			expect(bodyObj[0].name).toEqual("Women");

    		done();
    	})
  });

});

describe('Accessing People API', function() {
   it('should return the first person', function(done) {
    request.get({ url: baseUrl + '/person/1'},
    	function(error, response, body) {
    			var bodyObj = JSON.parse(body);
    			expect(bodyObj[0].name).toEqual("Morgan Freeman");
    		//	console.log(body);
    		done();
    	})
  });
});

describe('Accessing Industry API', function() {
   it('should return the first industry', function(done) {
    request.get({ url: baseUrl + '/industry/1'},
    	function(error, response, body) {
    			var bodyObj = JSON.parse(body);
    			expect(bodyObj[0].name).toEqual("Arts and Entertainment");
    			//console.log(body);
    		done();
    	})
  });
});
