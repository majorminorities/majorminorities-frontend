import React from 'react';
import loading from'./images/loading.gif';
import $ from 'jquery';
import PersonCard from './personCard.jsx';
import IndustryCard from './industryCard.jsx';
import MinorityCard from './minorityCard.jsx';
import SearchResultsCarousel from './searchResultsCarousel.jsx';

class SearchResults extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      people: null,
      industries: null,
      minorities: null,
    };
  }

  componentDidMount() {
    this.loadData();
  }

  loadData() {
    var url = "http://api.majorminorities.me/person";
    var peopleData;
    var minoritiesData;

    $.ajax({
      url      : url,
      dataType : 'json',
      type     : 'GET',
      async: false ,

      success: data => {
        peopleData = this.convertBirthDates(data);
      },  
      error: (xhr, status, err) => {
        console.error(this.props.url, status, err.toString());
      }});

      url = "http://api.majorminorities.me/minority";

      $.ajax({
        url      : url,
        dataType : 'json',
        type     : 'GET',
        async: false ,

        success: data => {
          minoritiesData = data;
        },
        error: (xhr, status, err) => {
          console.error(this.props.url, status, err.toString());
        }});

      url = "http://api.majorminorities.me/industry";

      $.ajax({
        url      : url,
        dataType : 'json',
        type     : 'GET',
        async: false ,

        success: data => {
          this.setState({
            people: peopleData,
            industries:data,
            minorities:minoritiesData,

          });
        },
        error: (xhr, status, err) => {
          console.error(this.props.url, status, err.toString());
        }});
  }

  getPeopleResults=  () => {
    var query = this.props.name;
    let people = this.state.people.filter((person) => {
      return person.name.toLowerCase().includes(query) || person.birth_date.includes(query) || person.birth_place.toLowerCase().includes(query)
        || person.minority_1.toLowerCase().includes(query) || person.minority_2.toLowerCase().includes(query) ||
        person.industry.toLowerCase().includes(query) || person.occupation.toLowerCase().includes(query)
    });
    var array = [];
    for (var x = 0; x < people.length; x++) {
      array.push(
        <PersonCard image={people[x].image}
                    name={people[x].name}
                    birth_date={people[x].birth_date}
                    birth_place={people[x].birth_place}
                    minority_1={people[x].minority_1}
                    minority_2={people[x].minority_2}
                    occupation={people[x].occupation}
                    industry={people[x].industry}
                    filter={[query]}/>
      );
    }
    return array;
  };

  getIndustryResults=  () => {
    var query = this.props.name;
    let industries = this.state.industries.filter((industry) => {
      return industry.name.toLowerCase().includes(query) ||
        industry.avg_salary.toString().includes(query) ||
        industry.top_fields.toLowerCase().includes(query) ||
        industry.div_score.toString().includes(query) }
      );
    var array = [];
    for (var x = 0; x < industries.length; x++) {
      array.push(
        <IndustryCard image={industries[x].image}
                      name={industries[x].name}
                      avgSalary={industries[x].avg_salary}
                      divScore={parseFloat(industries[x].div_score).toFixed(1)}
                      topFields={industries[x].top_fields}
                      repWo={parseFloat(industries[x].rep_women).toFixed(1)}
                      repAfr={parseFloat(industries[x].rep_afr).toFixed(1)}
                      filter={[query]}/>
      );
    }
    return array;
  };

  getMinorityResults=  () => {
    var query = this.props.name.toLowerCase();
    let minorities = this.state.minorities.filter((minority) => {
      return minority.name.toLowerCase().includes(query) ||
        minority.percentage_us.toString().includes(query)
    });
    var array = [];
    for (var x = 0; x < minorities.length; x++) {
      array.push(
        <MinorityCard image={minorities[x].image}
                      name={minorities[x].name}
                      uspercent={minorities[x].percentage_us}
                      percentages={minorities[x].percentages}
                      filter={[query]}/>

      );
    }
    return array;
  };

  convertBirthDates(data) {
    var months = {jan:'01',feb:'02',mar:'03',apr:'04',may:'05',jun:'06',
                  jul:'07',aug:'08',sep:'09',oct:'10',nov:'11',dec:'12'};
    for (var x = 0; x< data.length; x++) {
      var p = data[x].birth_date.substring(5,16).split(' ');
      data[x].birth_date = "" + p[2]+ '-' +  months[p[1].toLowerCase()]+ '-' +  p[0];
    }
    return data;
  }

  render() {
    if (!this.state.people ||!this.state.industries ||!this.state.minorities ) {
      return (
        <div>
          <h1 style={{display: 'flex', justifyContent:'center', alignItems:'center', marginTop: '10%'}}> Loading... </h1>
          <img src={loading} style={{display:'block', margin:'auto'}}/>
        </div>);
    }

    if(this.getPeopleResults().length < 1 && this.getMinorityResults().length < 1 && this.getIndustryResults().length < 1) {
      return (
        <div>
          <h1 style={{display: 'flex', justifyContent:'center', alignItems:'center'}}> Search Results for: "{this.props.name}" </h1>
          <h3 style={{display: 'flex', justifyContent:'center', alignItems:'center', marginTop: '10%'}}>
            There are no results for your search.
          </h3>
        </div>
      );
    }

    return (
      <div>
        <h1 style={{display: 'flex', justifyContent:'center', alignItems:'center'}}> Search Results for: "{this.props.name}" </h1>
        <SearchResultsCarousel
          cards={this.getPeopleResults()}
          type={"People"}
          searchResultsPage={true}/>
        <SearchResultsCarousel
          cards={this.getMinorityResults()}
          type={"Minorities"}
          searchResultsPage={true}/>
        <SearchResultsCarousel
          cards={this.getIndustryResults()}
          type={"Industries"}
          searchResultsPage={true}/>
      </div>
    )
  }
}

export default SearchResults;
