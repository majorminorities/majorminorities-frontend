from selenium import webdriver
import unittest
from selenium.webdriver.common.keys import Keys


 
class MajorMinoritiesTest(unittest.TestCase):
 
	@classmethod
	def setUpClass(cls):
		cls.driver = webdriver.Chrome()
		cls.driver.get('http://majorminorities.me/')
		cls.driver.implicitly_wait(45)
 
	#Tests for Clicking Links

	def test_title(self):
		driver = self.driver
		driver.get('http://majorminorities.me/')
		self.assertEqual(
			self.driver.title,
			'MajorMinorities')

	def test_click_industry_on_people_instance(self):
		driver = self.driver
		driver.get('http://majorminorities.me/person/Anita%20Borg')
		industry_link = driver.find_element_by_link_text('Industry')
		industry_link.click()

		self.assertEqual(
			driver.current_url,
			'http://majorminorities.me/industries')



	def test_click_minority_on_industry_instance(self):
		driver = self.driver
		driver.get('http://majorminorities.me/industry/Technology')
		industry_link = driver.find_element_by_link_text('Latinx')
		industry_link.click()

		self.assertEqual(
			driver.current_url,
			'http://majorminorities.me/minority/Latinx')


	def test_people_searching(self):
		driver = self.driver
		driver.get('http://majorminorities.me/people')
		search_field = driver.find_element_by_xpath('//input[@type="text"]')
		search_field.clear()
		search_field.send_keys("oprah")
		search_field.send_keys(Keys.RETURN)

		assert "No results found." not in driver.page_source		

	def test_minority_searching(self):
		driver = self.driver
		driver.get('http://majorminorities.me/minorities')
		search_field = driver.find_element_by_xpath('//input[@type="text"]')
		search_field.clear()
		search_field.send_keys("women")
		search_field.send_keys(Keys.RETURN)

		assert "No results found." not in driver.page_source

	def test_industry_searching(self):
		driver = self.driver
		driver.get('http://majorminorities.me/industries')
		search_field = driver.find_element_by_xpath('//input[@type="text"]')
		search_field.clear()
		search_field.send_keys("arts")
		search_field.send_keys(Keys.RETURN)

		assert "No results found." not in driver.page_source


	def test_click_gitlab_link(self):
		driver = self.driver
		driver.get('http://majorminorities.me/about')
		gitlab_link = driver.find_element_by_link_text('https://gitlab.com/majorminorities/majorminorities-frontend')
		gitlab_link.click()

		self.assertEqual(
			driver.current_url,
			'https://gitlab.com/majorminorities/majorminorities-frontend')


	# def test_last_name_sorting(self):
	# 	driver = self.driver
	# 	driver.get('http://majorminorities.me/people')
	# 	sort_button = driver.find_element_by_class_name('jss898')
	# 	sort_button.click()

	# 	self.assertEqual(
	# 		driver.current_url,
	# 		'http://majorminorities.me/people')




	@classmethod
	def tearDownClass(cls):
		cls.driver.quit() 
