import React from 'react';
import {
  Nav,
  NavItem,
  NavLink} from 'reactstrap';

class NavigationBar extends React.Component {
    constructor() {
      super();
    }

    render() {
      return (
        <Nav pullright="true">
          <NavItem componentclass='span'>
            <NavLink to="/responses">Responses</NavLink>
          </NavItem>
        </Nav>
      );
    }
}

export default NavigationBar;
