import React from 'react';
import SearchResultsCarousel from './searchResultsCarousel.jsx';
import PersonCard from './personCard.jsx';
import {HorizontalBar} from 'react-chartjs-2';
import { Row, Col } from 'reactstrap';

class Industry extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      data: null,
      people:null
    };
    this.minority_clicked = this.minority_clicked.bind(this);
  }

  minority_clicked(min) {
    var url = "minority.html?minority=" + min;
    window.location.href=url;
  }

  componentDidMount() {
    this.loadData();
  }

  clickedMinority(event) {
    window.location.pathname = '/minority/' + event[0]._model.label;
  }

  loadData() {
    var industryData = [];
    var url = "http://api.majorminorities.me/industry";

    fetch(url)
      .then(res => res.json())

      .then(contents => industryData = contents)
      .catch(() => console.log("Can’t access " + url + " response. Blocked by browser?"));

      url = "http://api.majorminorities.me/person";

      fetch(url)
        .then(res => res.json())

        .then(contents => {
          var data = this.convertBirthDates(contents);
          this.setState({
          data: industryData,
          people: data
        })})
        .catch(() => console.log("Can’t access " + url + " response. Blocked by browser?"));
  }

  getPeople = (name) => {
    let people = this.state.people.filter((person) => {
      return person.industry.toLowerCase().includes(name.toLowerCase())
    });
    var array = [];
    for (var x = 0; x < people.length; x++) {
      array.push(
        <PersonCard image={people[x].image}
                    name={people[x].name}
                    birth_date={people[x].birth_date}
                    birth_place={people[x].birth_place}
                    minority_1={people[x].minority_1}
                    minority_2={people[x].minority_2}
                    occupation={people[x].occupation}
                    industry={people[x].industry}
                    filter={['']}/>
      );
    }
    return array;
  };

  getMinorityNames(industry) {
    var percentages = industry.percentages;
    var minorityNames = []

    for(var x = 0; x < Object.keys(percentages).length; x++){
      minorityNames.push(Object.keys(percentages)[x]);
    }

    return  minorityNames;
  }

  getMinorityPercentage(industry) {
    var percentages = industry.percentages;

    var percentNumbers = []

    for(var x = 0; x < Object.keys(percentages).length; x++){
      percentNumbers.push(percentages[Object.keys(percentages)[x]]);
    }

    return  percentNumbers;
  }

  convertBirthDates(data) {
    var months = {jan:'01',feb:'02',mar:'03',apr:'04',may:'05',jun:'06',
                  jul:'07',aug:'08',sep:'09',oct:'10',nov:'11',dec:'12'};
    for(var x = 0; x< data.length; x++) {
      var p = data[x].birth_date.substring(5,16).split(' ');
      data[x].birth_date = "" + p[2]+ '-' +  months[p[1].toLowerCase()]+ '-' +  p[0];
    }
    return data;
  }

  render() {
    if (!this.state.data) {
      return (
        <div className="page-header" >
          <h1 style={{left:50, marginLeft:"15rem"}}> loading </h1>
        </div>
      );
    }
    var i = this.props.name;
    var industries = this.state.data;
    var industry;
    for (var x in industries) {
      if (industries[x].name == i)
        industry=industries[x];
    }
    if (!industry) {
      return (
        <div className="page-header" >
            <h1 style={{left:50, marginLeft:"15rem"}}> Oops! We don't have the information you requested. </h1>
        </div>
      );
    }
    var name = industry.name;
    var avgSalary = industry.avg_salary;
    var repAa = industry.rep_aa;
    var repAfr = industry.rep_afr;
    var repLa = industry.rep_latinx;
    var image = industry.image;
    var repWo = industry.rep_women;
    var divScore = industry.div_score;
    var topFields = industry.top_fields;
    var wikipedia = industry.external_link;
    var percentages = this.getMinorityPercentage(industry);
    var youtube_link = "https://www.youtube.com/embed/" + industry.media.substring(32);

    var labels = this.getMinorityNames(industry);

    const minorityRepresentationData = {
      labels: labels,
      datasets: [
        {
          label: 'Percent in industry',
          backgroundColor: 'rgba(255,99,132,0.2)',
          borderColor: 'rgba(255,99,132,1)',
          borderWidth: 1,
          hoverBackgroundColor: 'rgba(255,99,132,0.4)',
          hoverBorderColor: 'rgba(255,99,132,1)',
          data: percentages
        }
      ]
    };

    return (
      <main role="main" class="container">
        <div>
          <h1 style= {{display: 'flex', justifyContent:'center', alignItems:'center', marginBottom:'40px', marginTop:'40px'}}>{name} </h1>
          <div style= {{display: 'flex', justifyContent:'center', alignItems:'center'}}>
          <img style={{objectFit: 'cover', marginBottom:'60px'}} width="800" height="400" src={image}  alt={name}></img></div>
            <div style={{display: 'flex', justifyContent:'center', alignItems:'center', marginBottom:'50px'}}><iframe width="78%" height="530px" src={youtube_link}> </iframe></div>
          <Row style={{marginBottom:'60px'}}>
            <Col>
              <div style={{float:'right', marginRight:'10%'}}>
                <h1>Average Salary:</h1>
                <h3>${avgSalary} </h3>
              </div>
            </Col>
            <Col>
              <div style={{marginLeft:'10%'}}>
                <h1>Diversity Score:</h1>
                <h3>{divScore} </h3>
              </div>
            </Col>
          </Row>
          <h3 style={{display: 'flex', justifyContent:'center', alignItems:'center',marginTop:'80px'}}>
            Percentage of Various Minorities in {name} </h3>
          <h5 style={{display: 'flex', justifyContent:'center', alignItems:'center', marginBottom:'40px'}}>
            Click on the bar to learn more about each minority. </h5>
          <div style={{width:'1000px', marginLeft:'0', marginBottom:'-50px'}}>
            <HorizontalBar
              data={minorityRepresentationData}
              width={400}
              height={500}
              onElementsClick={((e) => this.clickedMinority(e))}
              options={{
                layout: {
                  padding: {
                    left: 50,
                    right: 50,
                    bottom: 50
                  }
                },
                scales: {
                  xAxes: [{
                    ticks: {
                      // Include a dollar sign in the ticks
                      callback: function(value) {
                        return  value + "%";
                      }
                    }
                  }]
                },
                maintainAspectRatio: false,
                scales: {
                  xAxes: [{
                    ticks: {
                      beginAtZero:true, suggestedMax: 100
                    }
                  }]
                }
              }}
            />
          </div>
          <h3 style={{display: 'flex', justifyContent:'center', alignItems:'center', marginTop:'60px'}}>
            Top Fields:</h3>
          <h5 style={{display: 'flex', justifyContent:'center', alignItems:'center', marginBottom:'60px'}}>
            {topFields} </h5>
          <h3 style={{display: 'flex', justifyContent:'center', alignItems:'center'}}>
            Notable Individuals</h3>
          <SearchResultsCarousel
            cards={this.getPeople(name)}
            type={""}
            searchResultsPage={false}
          />
          <Row style={{display: 'flex', justifyContent:'center', alignItems:'center', marginBottom:'40px'}}>
            <h3 style={{float:'right'}}>
              Learn more at:  </h3>
            <h3 style={{float:'left', marginLeft:'10px'}}>
              <a href={wikipedia}> {wikipedia}</a>
            </h3>
          </Row>
        </div>
      </main>
    )
  }
}

export default Industry;
