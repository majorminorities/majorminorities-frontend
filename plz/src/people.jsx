import React, { Component } from 'react';
import { Row, Col } from 'reactstrap';
import loading from'./images/loading.gif';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import $ from 'jquery';
import Chip from '@material-ui/core/Chip';
import Button from '@material-ui/core/Button';
import FilterIcon from '@material-ui/icons/FilterList';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import FormHelperText from '@material-ui/core/FormHelperText';
import ListItemText from '@material-ui/core/ListItemText';
import Select from '@material-ui/core/Select';
import Checkbox from '@material-ui/core/Checkbox';
import FormLabel from '@material-ui/core/FormLabel';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import Search from './Search'
import PersonCard from './personCard.jsx';
import PaginationGrid from './paginationGrid.jsx';

class People extends Component {
  constructor() {
    super();

    this.state = {
      data: null,
      filteredData:null,
      currentData: null,
      pageCount: null,
      searchFilter:[],
      offset: 0,
      filterData: {minorities:[], industries:[]},
      filter: {minority:[], industry:[], sort:'firstName', desc:false},
      filterChips:[],
      open: false,
    };

  }

  handlePageClick = (data) => {
    let selected = data.selected;
    let offset = Math.ceil(selected * 9);
    this.setState({offset: offset}, () => {
      this.updateShownPeople();
    });
  };

  updateShownPeople = () => {
    var pageData = [];
    var x;
    try {
      for (x = this.state.offset; x < (this.state.offset + 9); x++) {
        if (this.state.filteredData[x]) {
          pageData.push(this.state.filteredData[x]);
        }
      }
    }
    catch(exception) {}
    this.setState({currentData: pageData, pageCount: Math.ceil(this.state.filteredData.length / 9)}, this.updateFilterChips);
  };

  updateFilterChips() {
    var chips = [];
    var x;
    for (x = 0; x < this.state.filter.minority.length; x++) {
      chips.push(<Chip key={this.state.filter.minority[x]}
                       style={{marginRight: '1rem', marginBottom: '1rem'}}
                       label={this.state.filter.minority[x]}
                       onDelete={this.deleteMinority(this.state.filter.minority[x])} />);
    }
    if (this.state.filter.industry.length > 0) {
      chips.push(<Chip key={this.state.filter.industry}
                       style={{marginRight: '1rem', marginBottom: '1rem'}}
                       label={this.state.filter.industry}
                       onDelete={this.deleteIndustry(this.state.filter.industry)} />);
    }
    this.setState({filterChips: chips});
  }

  deleteMinority = minority => () => {
    console.log(this.state.filterChips);
    var chips = this.state.filterChips;
    var minorityFilter = this.state.filter.minority;
    var x;
    for (x = 0; x < chips.length; x++) {
      if (chips[x].key === minority) {
        chips.splice(x,1);
      }
    }
    for (var i = 0; i < this.state.filter.minority.length; i++) {
        if (this.state.filter.minority[i].match(minority))
          minorityFilter.splice(i,1);
    }
    this.setState({
      filter: {minority: minorityFilter, industry:this.state.filter.industry, sort: this.state.filter.sort, desc:this.state.filter.desc},
      filterChips: chips}
    , this.handleApplyFilter(null).bind(this));
  };

  deleteIndustry = industry => () => {
    var chips = this.state.filterChips;
    console.log(this.state.filter.industry);
    var x;
    for (x = 0; x < chips.length; x++) {
      if (chips[x].key === industry) {
        chips.splice(x,1);
      }
    }
    this.setState({
      filter: {minority: this.state.filter.minority, industry:[], sort: this.state.filter.sort, desc:this.state.filter.desc},
      filterChips: chips}
    , this.handleApplyFilter(null).bind(this));
  };

  getRepresentedMinorities(people) {
    var set = new Set();
    var x;
    for (x = 0; x < people.length; x++) {
      if (people[x].minority_1 != "None")
        set.add(people[x].minority_1);
      if(people[x].minority_2 != "None")
        set.add(people[x].minority_2);
    }
    return Array.from(set);
  }

  getRepresentedIndustries(people) {
    var set = new Set();
    var x;
    for (x = 0; x < people.length; x++) {
      set.add(people[x].industry);
    }
    return Array.from(set);
  }

  componentDidMount() {
    this.loadData();
  }

  loadData() {
    const url = "http://api.majorminorities.me/person";
    $.ajax({
      url      : url,
      data     : {limit: 9, offset: this.state.offset},
      dataType : 'json',
      type     : 'GET',

    success: data => {
      this.convertBirthDates(data);
      var dataSorted = this.sortData(data);
      console.log(dataSorted);
      var pageData = [];
      var x;
      try {
        for (x = this.state.offset; x < (this.state.offset + 9); x++) {
          pageData.push(dataSorted[x]);
        }
      }
      catch(exception) {}
      this.setState({
        data: dataSorted,
        filteredData:dataSorted,
        currentData:pageData,
        pageCount: Math.ceil(data.length / 9),
        filterData:{minorities: this.getRepresentedMinorities(data),
        industries: this.getRepresentedIndustries(data)}
      }, );
    },
    error: (xhr, status, err) => {
      console.error(this.props.url, status, err.toString());
    }});

  }

  convertBirthDates(data) {
    var months = {jan:'01',feb:'02',mar:'03',apr:'04',may:'05',jun:'06',
                  jul:'07',aug:'08',sep:'09',oct:'10',nov:'11',dec:'12'};
    for (var x = 0; x< data.length; x++) {
      var p = data[x].birth_date.substring(5,16).split(' ');
      console.log(p);
      data[x].birth_date = "" + p[2]+ '-' +  months[p[1].toLowerCase()]+ '-' +  p[0];
    }
    return data;
  }

  handleClickOpen = () => () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  filterMinorities() {
    var newData = [];
    var x;
    if (this.state.filter.minority.length > 1) {
      for (x = 0; x<this.state.data.length; x++){
        if (this.state.filter.minority.indexOf(this.state.data[x].minority_1) > -1 &&
            this.state.filter.minority.indexOf(this.state.data[x].minority_2) > -1 ) {
          newData.push(this.state.data[x]);
        }
      }
    } else {
      for (x = 0; x<this.state.data.length; x++){
        if (this.state.filter.minority.indexOf(this.state.data[x].minority_1) > -1 ||
            this.state.filter.minority.indexOf(this.state.data[x].minority_2) > -1) {
          newData.push(this.state.data[x]);
        }
      }
    }
    return newData;
  }

  filterIndustries(data) {
    var newData = [];
    var x;
    if (this.state.filter.industry.length > 0) {
      for (x = 0; x<data.length; x++) {
        if (this.state.filter.industry.indexOf(data[x].industry) > -1 ) {
          newData.push(data[x]);
        }
      }
    } else {
      newData = data;
    }
    return newData;
  }

  sortData(data) {
    var newData = data;
    switch (this.state.filter.sort) {
      case "firstName":
        newData.sort(function(a, b) {
          if(a.name < b.name) { return -1; }
          if(a.name > b.name) { return 1; }
          return 0;
        })
        break;
      case "lastName":
        newData.sort(function(a, b) {
          var aSplit = a.name.split(" ");
          var bSplit = b.name.split(" ");
          console.log(aSplit);
          console.log(aSplit[aSplit.length-1]);
          if(aSplit[aSplit.length-1] < bSplit[bSplit.length-1]) { return -1; }
          if(aSplit[aSplit.length-1] > bSplit[bSplit.length-1]) { return 1; }
          return 0;
        })
        break;
      case "birthDate":
        newData.sort(function(a, b) {
          if(a.birth_date < b.birth_date) { return -1; }
          if(a.birth_date > b.birth_date) { return 1; }
          return 0;
        })
        break;
      default:
    }

    if  (this.state.filter.desc) {
      return newData.reverse();
    } else {
      return newData;
    }
  }

  handleApplyFilter = error => () => {
    var newData = [];
    if (error) {
    } else {
      this.handleClose();
      if (this.state.filter.minority.length < 1) {
        newData = this.state.data;
      } else {
        newData = this.filterMinorities();
      }
      if (this.state.filter.industry.length > 1) {
        newData = this.filterIndustries(newData);
      }
      newData = this.sortData(newData);
      console.log(newData);
      this.setState({filteredData: newData, offset:0}, this.updateShownPeople);
    }
  };

  handleChangeMinority = event => {
    this.setState({filter: {minority: event.target.value, industry:this.state.filter.industry,sort: this.state.filter.sort, desc:this.state.filter.desc}});
  };

  handleChangeIndustry = event => {
    if (this.state.filter.industry.indexOf(event.target.value) > -1) {
      this.setState({filter: {minority: this.state.filter.minority, industry:[], sort: this.state.filter.sort, desc:this.state.filter.desc}});
    } else {
      this.setState({filter: {minority: this.state.filter.minority, industry:event.target.value, sort: this.state.filter.sort, desc:this.state.filter.desc}});
    }

  };

  handleSortChange = event => {
    console.log(event.target.value);
    this.setState({ filter: {minority:this.state.filter.minority, industry:this.state.filter.industry, sort: event.target.value, desc:this.state.filter.desc} });
  };

  handleSortOrderChange = event => {
    console.log(event.target.checked);
    this.setState({ filter: {minority:this.state.filter.minority, industry:this.state.filter.industry, sort: this.state.filter.sort, desc:event.target.checked} });
  };

  searchCards(query) {
    let people = this.state.data.filter((person) => {
      return person.name.toLowerCase().includes(query) || person.birth_date.includes(query) || person.birth_place.toLowerCase().includes(query)
        || person.minority_1.toLowerCase().includes(query) || person.minority_2.toLowerCase().includes(query) ||
        person.industry.toLowerCase().includes(query) || person.occupation.toLowerCase().includes(query)
    });
    this.setState({filteredData:people, searchFilter:[query], filter: {minority:[], industry:[], sort:'firstName', desc:false}}, this.updateShownPeople);
    this.updateFilterChips();
  }

  render() {
    console.log(this.state);
    if (!this.state.data) {
      return (
        <div>
          <h1 style={{display: 'flex', justifyContent:'center', alignItems:'center', marginTop: '10%'}}> Loading... </h1>
          <img src={loading} style={{display:'block', margin:'auto'}}/>
        </div>);
    }
    const error = this.state.filter.minority.filter(v => v).length > 2;
    return (
      <div>
        <div className="page-header">
          <h1 style={{fontSize: "6rem", marginTop: "3rem", marginBottom:'3rem', display: 'flex', justifyContent:'center'}}>People </h1>
        </div>
        <div style={{ display: 'flex'}} >
          <div id ='search' style={{ marginLeft:'11rem', float:'left'}}><Search searchCards={this.searchCards.bind(this)}/></div>
          <div style={{ marginLeft:'11rem', float:'left', width:'100%'}}>
            {this.state.filterChips}
          </div>
          <div style={{float:'right'}}>
            <Button variant="contained" color="default"
                    onClick={this.handleClickOpen('paper')}
                    style={{float:'right', marginLeft:'3rem', marginRight:'11rem', verticalAlign:'middle', fontSize:'13px'}} >
              Filter/Sort
              <FilterIcon/>
            </Button>
          </div>
          <Dialog
            open={this.state.open}
            onClose={this.handleClose}
            aria-labelledby="scroll-dialog-title">
            <DialogContent>
              <Row style={{marginBottom:'.5rem'}}>
                <Col>
                  <FormLabel style={{fontSize:'1.5rem'}}>Filter by:</FormLabel>
                </Col>
                <Col>
                  <FormLabel style={{fontSize:'1.5rem'}}>Sort by:</FormLabel>
                </Col>
              </Row>
                <form style={{display: 'flex',flexWrap: 'wrap'}}>
                <Row>
                  <Col>
                    <FormControl error={error} component="fieldset" style={{margin: 'theme.spacing.unit',minWidth: 140}}>
                    <InputLabel htmlFor="age-native-label-placeholder">Minority</InputLabel>
                    <Select
                      multiple
                      value={this.state.filter.minority}
                      onChange={this.handleChangeMinority}
                      input={<Input id="select-multiple-checkbox" />}
                      renderValue={selected => selected.join(', ')}>
                      {this.state.filterData.minorities.map(minority => (
                        <MenuItem key={minority} value={minority}>
                          <Checkbox checked={this.state.filter.minority.indexOf(minority) > -1} />
                          <ListItemText primary={minority} />
                        </MenuItem>
                      ))}
                    </Select>
                    <FormHelperText>Select up to 2 minorities.</FormHelperText>
                    </FormControl>
                    <FormControl style={{margin: 'theme.spacing.unit',minWidth: 140, marginTop:"10px"}}>
                      <InputLabel htmlFor="age-native-label-placeholder">Industry</InputLabel>
                        <Select
                          value={this.state.filter.industry}
                          onChange={this.handleChangeIndustry}
                          input={<Input id="select-multiple-checkbox" />}
                          renderValue={selected => selected}>
                          {this.state.filterData.industries.map(industry => (
                            <MenuItem key={industry} value={industry}>
                              <Checkbox checked={this.state.filter.industry.indexOf(industry) > -1} />
                              <ListItemText primary={industry} />
                            </MenuItem>
                          ))}
                        </Select>
                        <FormHelperText>Select up to 1 industry.</FormHelperText>
                    </FormControl>
                  </Col>
                <Col>
                  <FormControl>
                    <RadioGroup
                      value={this.state.filter.sort}
                      onChange={this.handleSortChange}
                      style={{margin:'${theme.spacing.unit}px 0'}}>
                      <FormControlLabel value="firstName" control={<Radio color="primary"/>} label="First name alphabetically" />
                      <FormControlLabel value="lastName" control={<Radio color="primary"/>} label="Last name alphabetically" style={{marginTop:'-15px', minWidth: 165}}/>
                      <FormControlLabel value="birthDate" control={<Radio color="primary"/>} label="Date of birth" style={{marginTop:'-15px', minWidth: 165}}/>
                    </RadioGroup>
                    <FormControlLabel
                      control={
                        <Switch
                          checked={this.state.filter.desc}
                          onChange={this.handleSortOrderChange}
                          color="primary"/>
                      }
                      label="Descending order"/>
                  </FormControl>
                </Col>
              </Row>
            </form>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleClose} color="primary">
              Cancel
            </Button>
            <Button id='applyFilter' onClick={this.handleApplyFilter(error).bind(this)} color="primary">
              Apply
            </Button>
          </DialogActions>
        </Dialog>
      </div>
      <div style={{display: 'flex', justifyContent:'center', alignItems:'center'}}>
        <PaginationGrid data={
          this.state.currentData.map((person, index) => {
            if(person) {
              return (
                <div key={index}>
                  <PersonCard image={person.image}
                              name={person.name}
                              birth_date={person.birth_date}
                              birth_place={person.birth_place}
                              minority_1={person.minority_1}
                              minority_2={person.minority_2}
                              occupation={person.occupation}
                              industry={person.industry}
                              filter={this.state.searchFilter}/>
                </div>
              );
            } else {
              return;
            }
          })} searchFilter={this.state.searchFilter}
              pageCount = {this.state.pageCount}
              handlePageClick={this.handlePageClick.bind(this)}/>
      </div>
      </div>
    )
  }
}

export default People;
