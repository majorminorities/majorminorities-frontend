import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Card, {
  } from "@material/react-card";
import '@material/react-card/dist/card.css';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import CardMedia from '@material-ui/core/CardMedia';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Highlighter from "react-highlight-words";

class IndustryCard extends Component {

  render() {

    const textStyle = {
      marginTop:'1rem',
      textIndent: '-6px'
    }

    const reduceBulletTextGap = {
      marginLeft: '-6px'
    }

    var industry = this.props;
        return (
          <div>
          <Card style={{margin:"3rem", width: '280px'}}>
            <CardMedia component="img"
                       image={industry.image}
                       style={{height: '22rem', width: '280px', objectFit: 'cover'}}/>
            <CardContent style={{height: '27rem'}}>
              <Typography gutterBottom variant="display2" style={{fontSize:'18px', textIndent: '-6px', color:"#000000"}}>
              <Highlighter
                highlightClassName="HighlightClass"
                searchWords={industry.filter}
                autoEscape={true}
                textToHighlight={industry.name}
              />
              </Typography>
              <Typography component="p" style={{fontSize:"12px", height:'9rem'}}>
                <li style={textStyle}><span style={reduceBulletTextGap}>Average Salary: ${industry.avgSalary} </span></li>
                <li style={textStyle}><span style={reduceBulletTextGap}>Top fields: <Highlighter
                  highlightClassName="HighlightClass"
                  searchWords={industry.filter}
                  autoEscape={true}
                  textToHighlight={industry.topFields}
                /> </span></li>
                <li style={textStyle}><span style={reduceBulletTextGap}>Diversity Score: {industry.divScore} </span></li>
                <li style={textStyle}><span style={reduceBulletTextGap}>Representation of {Object.keys(industry.percentages)[0]}: {industry.percentages[Object.keys(industry.percentages)[0]]}% </span></li>
                <li style={textStyle}><span style={reduceBulletTextGap}>Representation of {Object.keys(industry.percentages)[1]}: {industry.percentages[Object.keys(industry.percentages)[1]]}% </span></li>
              </Typography>
            </CardContent>
            <CardActions>
              <Link to={'/industry/' + industry.name} style={{textDecoration:'none'}}>
                <Button size="small" color="primary" style={{fontSize:"1.3rem", marginLeft:"5", marginBottom:"5"}}>
                  Learn More
                </Button>
              </Link>
            </CardActions>
          </Card>
          </div>);
    }
};

export default IndustryCard;
