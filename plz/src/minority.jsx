import React from 'react';
import { Redirect } from 'react-router-dom';
import {Doughnut, HorizontalBar} from 'react-chartjs-2';
import { Row, Col } from 'reactstrap';
import SearchResultsCarousel from './searchResultsCarousel.jsx';
import PersonCard from './personCard.jsx';


class Minority extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      data: null, //This is what our data will eventually be loaded into
      people:null,
      redirect: {shouldRedirect: false}
    };
  }

  componentDidMount() {
    this.loadData();
  }

  loadData() {
    let minorityJSON;
    var minorityData = [];
    var url = "http://api.majorminorities.me/minority";

    fetch(url)
      .then(res => res.json())
      .then(data => minorityJSON = data)
      .then(contents => minorityData = contents)
      .catch(() => console.log("Can’t access " + url + " response. Blocked by browser?"));

      url = "http://api.majorminorities.me/person";

      fetch(url)
        .then(res => res.json())
        .then(data => minorityJSON = data)
        .then(contents => {
          var data = this.convertBirthDates(contents);
          this.setState({
          data: minorityData,
          people: data
        })})
        .catch(() => console.log("Can’t access " + url + " response. Blocked by browser?"));
  }

  clickedIndustry(event) {
    window.location.pathname = '/industry/' + event[0]._model.label;
  }

  convertBirthDates(data) {
    var months = {jan:'01',feb:'02',mar:'03',apr:'04',may:'05',jun:'06',
                  jul:'07',aug:'08',sep:'09',oct:'10',nov:'11',dec:'12'};
    for (var x = 0; x < data.length; x++) {
      var p = data[x].birth_date.substring(5,16).split(' ');
      data[x].birth_date = "" + p[2]+ '-' +  months[p[1].toLowerCase()]+ '-' +  p[0];
    }
    return data;
  }

  getIndustriesNames(minority) {
    var percentages = minority.percentages;
    var industryNames = [];

    for (var x = 0; x < Object.keys(percentages).length; x++) {
      industryNames.push(Object.keys(percentages)[x]);
    }

    return industryNames;
  }

  getIndustriesPercentage(minority) {
    var percentages = minority.percentages;
    var percentNumbers = [];

    for (var x = 0; x < Object.keys(percentages).length; x++) {
      percentNumbers.push(percentages[Object.keys(percentages)[x]]);
    }

    return percentNumbers;
  }

  getPeople = (name) => {
    let people = this.state.people.filter((person) => {
      return  person.minority_1.toLowerCase().includes(name.toLowerCase()) || person.minority_2.toLowerCase().includes(name.toLowerCase())
    });
    var array = [];
    for (var x = 0; x < people.length; x++) {
      array.push(
        <PersonCard image={people[x].image}
                    name={people[x].name}
                    birth_date={people[x].birth_date}
                    birth_place={people[x].birth_place}
                    minority_1={people[x].minority_1}
                    minority_2={people[x].minority_2}
                    occupation={people[x].occupation}
                    industry={people[x].industry}
                    filter={['']}/>
      );
    }
    return array;
  };

  render() {
    if (this.state.redirect.shouldRedirect) {
      return <Redirect to={this.state.redirect.redirectLink}/>
    }

    if (!this.state.data) {
      return (
        <div className="page-header" >
          <h1 style={{left:50, marginLeft:"15rem"}}> loading </h1>
        </div>
      );
    }
    var m = this.props.name;
    var minorities = this.state.data;
    var minority;
    for (var x in minorities) {
      if (minorities[x].name == m)
        minority=minorities[x];
    }
    if (!minority) {
      return (
        <div className="page-header" >
          <h1 style={{left:50, marginLeft:"15rem"}}> Oops! We don't have the information you requested. </h1>
        </div>
      );
    }

    var name = minority.name;
    var uspercent = minority.percentage_us;
    var originCountries = minority.origin_countries;
    var image = minority.image;
    var wikipedia = minority.external_link;
    var youtube_link = "https://www.youtube.com/embed/" + minority.media.substring(32);
    var percentages = this.getIndustriesPercentage(minority);
    var labels = this.getIndustriesNames(minority);

    const usPercentData = {
    	labels: [
    		name,
    		'Non ' + name
    	],
    	datasets: [{
    		data: [uspercent, 100-uspercent],
    		backgroundColor: [
    		'#FF6384',
    		'#D3D3D3',
    		'#FFCE56'
    		],
    		hoverBackgroundColor: [
    		'#FF6384',
    		'#D3D3D3',
    		'#FFCE56'
    		]
    	}]
    };

    const industryRepresentationData = {
      labels: labels,
      datasets: [
        {
          label: 'Percent in industry',
          backgroundColor: 'rgba(255,99,132,0.2)',
          borderColor: 'rgba(255,99,132,1)',
          borderWidth: 1,
          hoverBackgroundColor: 'rgba(255,99,132,0.4)',
          hoverBorderColor: 'rgba(255,99,132,1)',
          data: percentages
        }
      ]
    };

    return (
      <main role="main" className="container">
        <div>
          <h1 style= {{display: 'flex', justifyContent:'center', alignItems:'center', marginBottom:'40px', marginTop:'40px'}}>{name} </h1>
          <div style= {{display: 'flex', justifyContent:'center', alignItems:'center'}}>
            <img style={{objectFit: 'cover', marginBottom:'40px'}} width="700" height="500" src={image}  alt={name}></img>
          </div>
          <div style={{display: 'flex', justifyContent:'center', alignItems:'center', marginBottom:'50px'}}><iframe width="78%" height="530px" src={youtube_link}> </iframe></div>
          <h3 style= {{display: 'flex', justifyContent:'center', alignItems:'center', marginBottom:'30px', marginTop:'80px'}}>
            Percentage of U.S. Population 
          </h3>
          <Row>
            <Col>
              <div style={{flex:1, justifyContent:'center', alignItems:'center', marginLeft: 'auto', marginRight: 'auto', width:'400px'}}>
                <Doughnut data={usPercentData}  width={300} height={300}/>
              </div>
            </Col>
          </Row>
          <h3 style= {{display: 'flex', justifyContent:'center', alignItems:'center',marginTop:'80px'}}>
            Percentage in Various Industries 
          </h3>
          <h5 style= {{display: 'flex', justifyContent:'center', alignItems:'center', marginBottom:'40px'}}>
            Click on the bar to learn more about each industry. 
          </h5>
          <Row>
            <div style={{width:'1100px', marginLeft:'0', marginBottom:'-50px'}}>
              <HorizontalBar
                data={industryRepresentationData}
                width={500}
                height={500}
                onElementsClick={((e) => this.clickedIndustry(e))}
                options={{
                  layout: {
                    padding: {
                      left: 50,
                      right: 50,
                      bottom: 50
                    }
                  },
                  scales: {
                    xAxes: [{
                      ticks: {
                        // Include a dollar sign in the ticks
                        callback: function(value) {
                          return  value + "%";
                        }
                      }
                    }]
                  },
                maintainAspectRatio: false,
                scales: {
                  xAxes: [{
                    ticks: {
                      beginAtZero:true, suggestedMax: 100
                  }
                  }]
                }
              }}
            />
          </div>
        </Row>
        <h3 style= {{display: 'flex', justifyContent:'center', alignItems:'center',marginTop:'80px'}}>
          Top country(s) of Origin: 
        </h3>
        <h5 style= {{display: 'flex', justifyContent:'center', alignItems:'center', marginBottom:'40px'}}>
          {originCountries} 
        </h5>
        <h3 style={{display: 'flex', justifyContent:'center', alignItems:'center'}}>
          Notable Individuals 
        </h3>
        <SearchResultsCarousel
          cards={this.getPeople(name)}
          type={""}
          searchResultsPage={false}/>
        <Row style={{display: 'flex', justifyContent:'center', alignItems:'center'}}>
          <h3 style={{float:'right'}}>
            Learn more at:  
          </h3>
          <h3 style={{float:'left', marginLeft:'10px'}}>
            <a href={wikipedia}> {wikipedia}</a> 
          </h3>
        </Row>
        <div style={{marginBottom:'50px'}}></div>
        </div>
      </main>
    )
  }
}

export default Minority;
