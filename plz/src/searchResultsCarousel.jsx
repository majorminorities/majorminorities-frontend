import React from 'react';
import InfiniteCarousel from 'react-leaf-carousel';

class SearchResultsCarousel extends React.Component {

  getHeadline() {
    if (this.props.cards.length > 0 && this.props.searchResultsPage) {
      return "" + this.props.type + " Results";
    } else {
      return;
    }
  }

  render() {
    return (
      <div>
        <h3 style={{marginLeft:'85px', marginBottom:'-20px'}}> {this.getHeadline()} </h3>
        <div>
          <InfiniteCarousel
            breakpoints={[{
              breakpoint: 500,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 2,
              },
            },
            {
            breakpoint: 768,
              settings: {
                slidesToShow: 3,
                slidesToScroll: 3,
              },
            },
            ]}
            dots={true}
            showSides={false}
            sidesOpacity={.5}
            sideSize={.1}
            slidesToShow={3}
            slidesToScroll={3}
            slidesSpacing={0}>
            {this.props.cards}
          </InfiniteCarousel>
        </div>
      </div>
    );
  }
};

export default  SearchResultsCarousel;
