import React, { Component } from 'react';
import './App.css';
import ReactDOM from 'react-dom';
import People from './people.jsx';
import Industries from './industries.jsx';
import Minorities from './minorities.jsx';
import Visualizations from './visualizations.jsx';
import About from './about.jsx';
import Person from './person.jsx';
import Minority from './minority.jsx';
import Industry from './industry.jsx';
import SearchResults from './searchResults.jsx';
import Navigationbar from './Navigationbar.js';
import { BrowserRouter as Router, Route } from "react-router-dom";
import minorities from'./images/minorities.jpg';
import women from'./images/women.jpg';
import industries from'./images/industries.jpg';
import  {Carousel, CarouselCaption, CarouselInner, CarouselItem, View, Mask } from 'mdbreact';
import '@material/react-card/dist/card.css';
import $ from 'jquery';
import loading from'./images/loading.gif';
import { Link } from 'react-router-dom';
import PersonCard from './personCard.jsx';

class App extends Component {

  constructor() {
    super();

    this.state = {
      spotlight: null
    };
  }

  componentDidMount() {
    document.title = "MajorMinorities"
  }

  componentWillMount() {
    const url = "http://api.majorminorities.me/spotlight";
    $.ajax({
      url      : url,
      dataType : 'json',
      type     : 'GET',

      success: data => {
        data = this.convertBirthDates(data);
        this.setState({
          spotlight: data[0]
        });

      },
      error: (xhr, status, err) => {
        console.error(this.props.url, status, err.toString());
      }});
  }

  convertBirthDates(data) {
    var months = {jan:'01',feb:'02',mar:'03',apr:'04',may:'05',jun:'06',
                  jul:'07',aug:'08',sep:'09',oct:'10',nov:'11',dec:'12'};
    for(var x = 0; x< data.length; x++) {
      var p = data[x].birth_date.substring(5,16).split(' ');
      data[x].birth_date = "" + p[2]+ '-' +  months[p[1].toLowerCase()]+ '-' +  p[0];
    }
    return data;
  }

  render() {
    if (!this.state.spotlight) {
      return (
        <div>
          <h1 style={{display: 'flex', justifyContent:'center', alignItems:'center', marginTop: '10%'}}> Loading... </h1>
          <img src={loading} style={{display:'block', margin:'auto'}}/>
        </div>
      );
    }

    const Home = () => (
      <div>
        <div className="page-header">
          <h1 style={{fontSize: "3rem", marginTop: "3rem", display: 'flex', justifyContent:'center', alignItems:'center', textAlign:'center'}}>
            Celebrate the achievements of individuals in underrepresented groups!
          </h1>
          <h3 style={{fontSize: "3rem", marginTop: "3rem", marginBottom:'40px', display: 'flex', justifyContent:'center', alignItems:'center', textAlign:'center'}}>
            Click above to learn more.
          </h3>
        </div>
        <Carousel
          activeItem={1}
          length={3}
          showControls={true}
          showIndicators={false}
          style={style}>
          <CarouselInner>
            <CarouselItem itemId="1">
              <View>
                <img style={{
                  objectFit: 'cover',
                  objectPosition: '50% 5%',
                  height:'470px'}}
                  className="d-block w-100"
                  src={minorities}
                  alt="First slide" />
                <Mask overlay="black-light"></Mask>
              </View>
              <CarouselCaption>
                <h3 className="h3-responsive">People</h3>
                <Link to={'/people' } style={{textDecoration:'none', color:"inherit"}}>
                  <p>Click here to learn more about different minority people with significant achievements.</p>
                </Link>
              </CarouselCaption>
            </CarouselItem>
            <CarouselItem itemId="2">
              <View>
                <img className="d-block w-100" style={{
                  objectFit: 'cover',
                  objectPosition: '50% 5%',
                  height:'470px'}}
                  src={women}
                  alt="Second slide" />
                <Mask overlay="black-strong"></Mask>
              </View>
              <CarouselCaption>
                <h3 className="h3-responsive">Minorities</h3>
                <Link to={'/minorities' } style={{textDecoration:'none', color:"inherit"}}>
                  <p>Click here to learn more about different minority groups.</p>
                </Link>
              </CarouselCaption>
            </CarouselItem>
            <CarouselItem itemId="3">
              <View>
                <img className="d-block w-100" style={{
                  objectPosition: '50% 5%',
                  height:'470px'}}
                  src={industries}
                  alt="Second slide" />
              </View>
              <CarouselCaption>
                <h3 className="h3-responsive">Industries</h3>
                <Link to={'/industries' } style={{textDecoration:'none', color:"inherit"}}>
                  <p>Click here to learn more about representation in different industries.</p>
                </Link>
              </CarouselCaption>
            </CarouselItem>
          </CarouselInner>
        </Carousel>
        <div>
          <h3 style={{fontSize: "3rem", marginTop: "3rem", display: 'flex', justifyContent:'center', alignItems:'center'}}>Person of the week</h3>
        </div>
        <div style={{display: 'flex', justifyContent:'center', alignItems:'center', marginBottom:'50px'}}>
          <PersonCard
            image={this.state.spotlight.image}
            name={this.state.spotlight.name}
            birth_date={this.state.spotlight.birth_date}
            birth_place={this.state.spotlight.birth_place}
            minority_1={this.state.spotlight.minority_1}
            minority_2={this.state.spotlight.minority_2}
            occupation={this.state.spotlight.occupation}
            industry={this.state.spotlight.industry}
            filter={[]}/>
        </div>
      </div>
    );

    return (
      <Router>
        <div>
          <Navigationbar/>
          <Route exact path="/" component={Home} />
          <Route path="/people" component={People} />
          <Route path="/minorities" component={Minorities} />
          <Route path="/industries" component={Industries} />
          <Route path="/about" component={About} />
          <Route path="/visualizations" component={Visualizations} />
          <Route name="minority" path="/searchResults/:name" render={(props)=><SearchResults name={props.match.params.name}/>}/>
          <Route name="minority" path="/minority/:name" render={(props)=><Minority name={props.match.params.name}/>}/>
          <Route name="industry" path="/industry/:name" render={(props)=><Industry name={props.match.params.name}/>}/>
          <Route name="person" path="/person/:name" render={(props)=><Person name={props.match.params.name}/>}/>
        </div>
      </Router>
    );
  }
}

const style = {
  top: '-30px'
};

ReactDOM.render( <App/> , document.getElementById("root"));

export default App;
