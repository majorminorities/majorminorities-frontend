import React from 'react';
import { Row, Col } from 'reactstrap';
import PaginationGrid from './paginationGrid.jsx';
import IndustryCard from './industryCard.jsx';
import loading from'./images/loading.gif';
import $ from 'jquery';
import Chip from '@material-ui/core/Chip';
import Button from '@material-ui/core/Button';
import FilterIcon from '@material-ui/icons/FilterList';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import Search from './Search';
import 'rc-slider/assets/index.css';
import 'rc-tooltip/assets/bootstrap.css';

const Slider = require('rc-slider');
const createSliderWithTooltip = Slider.createSliderWithTooltip;
const Range = createSliderWithTooltip(Slider.Range);

class Industries extends React.Component {
  constructor() {
    super();

    this.state = {
      data: null,
      filteredData:null,
      currentData: null,
      pageCount: null,
      searchFilter:[],
      offset: 0,
      filter: {salary:[0, 200000], divScore:[0,100], sort:'name', desc:false},
      filterChips:[],
      open: false,
    };
  }

  handlePageClick = (data) => {
    let selected = data.selected;
    let offset = Math.ceil(selected * 9);

    this.setState({offset: offset}, () => {
      this.updateShownIndustries();
    });
  };

  updateFilterChips() {
    var chips = [];
    if (this.state.filter.salary[0] > 0 || this.state.filter.salary[1] < 200000) {
      var salaryLabel = "Salary: $" + this.state.filter.salary[0] + '-$' + this.state.filter.salary[1];
      chips.push(<Chip key='salary'
                       style={{marginRight: '1rem', marginBottom: '1rem'}}
                       label={salaryLabel}
                       onDelete={this.deleteSalaryRange} />);
    }
    if (this.state.filter.divScore[0] > 0 || this.state.filter.divScore[1] < 100) {
      var divScoreLabel = "Diversity score: " + this.state.filter.divScore[0] + '-' + this.state.filter.divScore[1];
      chips.push(<Chip key='divScore'
                       style={{marginRight: '1rem', marginBottom: '1rem'}}
                       label={divScoreLabel}
                       onDelete={this.deleteDivScoreRange} />);
    }
    this.setState({filterChips: chips});
  }

  deleteSalaryRange = () => {
    var chips = this.state.filterChips;
    for (var x = 0; x < chips.length; x++) {
      if (chips[x].key === 'salary') {
        chips.splice(x,1);
      }
    }
    this.setState({
      filter:  {salary:[0,200000], divScore:this.state.filter.divScore, sort: this.state.filter.sort, desc:this.state.filter.desc} ,
      filterChips: chips}
    , this.handleApplyFilter.bind(this));
  };

  deleteDivScoreRange = () => {
    var chips = this.state.filterChips;
    for (var x = 0; x < chips.length; x++) {
      if (chips[x].key === 'divScore') {
        chips.splice(x,1);
      }
    }
    this.setState({
      filter: {salary:this.state.filter.salary, divScore:[0,100], sort: this.state.filter.sort, desc:this.state.filter.desc} ,
      filterChips: chips}
    , this.handleApplyFilter.bind(this));
  };

  updateShownIndustries() {
    var pageData = [];
    var x;
    try {
      for (x = this.state.offset; x < (this.state.offset + 9); x++) {
        if (this.state.filteredData[x]) {
          pageData.push(this.state.filteredData[x]);
        }
      }
    }
    catch(exception) {}
    this.setState({currentData: pageData,pageCount: Math.ceil(this.state.filteredData.length / 9)}, this.updateFilterChips);
  }

  componentDidMount() {
    this.loadData();
  }

  loadData() {
    const url = "http://api.majorminorities.me/industry";
    $.ajax({
      url      : url,
      data     : {limit: 9, offset: this.state.offset},
      dataType : 'json',
      type     : 'GET',

      success: data => {
        var dataSorted = this.sortData(data);
        var pageData = []
        var x;
        try {
          for (x = this.state.offset; x < (this.state.offset + 9); x++) {
            pageData.push(dataSorted[x]);
          }
        }
        catch(exception) {}
        this.setState({
          data: dataSorted,
          filteredData:dataSorted,
          currentData:pageData,
          pageCount: Math.ceil(data.length / 9)
        });
      },

      error: (xhr, status, err) => {
        console.error(this.props.url, status, err.toString());
      }});
  }

  searchCards(query) {
    let industries = this.state.data.filter((industry) => {
      return industry.name.toLowerCase().includes(query) ||
        industry.avg_salary.toString().includes(query) ||
        industry.top_fields.toLowerCase().includes(query) ||
        industry.div_score.toString().includes(query) });

    this.setState({filteredData:industries, searchFilter:[query], filter: {salary:[0, 200000], divScore:[0,100], sort:'name', desc:false}}, this.updateShownIndustries);
  }

  handleClickOpen = () => () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  sortData(data) {
    var newData = data;
    switch (this.state.filter.sort) {
      case "name":
        newData.sort(function(a, b){
          if(a.name < b.name) { return -1; }
          if(a.name > b.name) { return 1; }
          return 0;
        })
        break;
      case "divScore":
        console.log("divscore");
        console.log(newData);
        newData.sort(function(a, b){
          if(a.div_score < b.div_score) { return -1; }
          if(a.div_score > b.div_score) { return 1; }
          return 0;
        })
        break;
      case "salary":
        newData.sort(function(a, b){
          if(a.avg_salary < b.avg_salary) { return -1; }
          if(a.avg_salary > b.avg_salary) { return 1; }
          return 0;
        })
        break;
      default:
    }

    if (this.state.filter.desc) {
      return newData.reverse();
    } else {
      return newData;
    }
  }

  handleSalaryRangeChange= value => {
    this.setState({ filter: {salary:value, divScore:this.state.filter.divScore, sort: this.state.filter.sort, desc:this.state.filter.desc} });
  };

  handleDivScoreRangeChange = value => {
    this.setState({ filter: {salary:this.state.filter.salary, divScore:value, sort: this.state.filter.sort, desc:this.state.filter.desc} });
  };

  handleSortOrderChange = event => {
    this.setState({ filter: {salary:this.state.filter.salary, divScore:this.state.filter.divScore, sort: this.state.filter.sort, desc:event.target.checked} });
  };

  handleSortChange = event => {
    this.setState({ filter: {salary:this.state.filter.salary, divScore:this.state.filter.divScore, sort: event.target.value, desc:this.state.filter.desc} });
  };

  handleApplyFilter = () => {
    var newData = [];
    this.handleClose();
    newData = this.filterSalary();
    newData = this.filterDivScore(newData);
    newData = this.sortData(newData);
    this.setState({filteredData: newData, offset:0}, this.updateShownIndustries);
  };

  filterSalary() {
    var newData=[];
    var x;
    for (x = 0; x<this.state.data.length; x++){
      if (this.state.data[x].avg_salary >= this.state.filter.salary[0] &&
        this.state.data[x].avg_salary <= this.state.filter.salary[1] ) {
        newData.push(this.state.data[x]);
      }
    }
    return newData;
  }

  filterDivScore(data) {
    var newData=[];
    var x;
    for (x = 0; x<data.length; x++){
      console.log(data[x].div_score);
      console.log(this.state.filter.divScore[0]);
      console.log(this.state.filter.divScore[1]);
      if(data[x].div_score >= this.state.filter.divScore[0] &&
        data[x].div_score <= this.state.filter.divScore[1] ) {
        newData.push(data[x]);
      }
    }
    console.log(newData);
    return newData;
  }

  render() {
    if (!this.state.data) {
      return (
        <div >
          <h1 style={{display: 'flex', justifyContent:'center', alignItems:'center', marginTop: '10%'}}> Loading... </h1>
          <img src={loading} style={{display:'block', margin:'auto'}}/>
        </div>
      );
    }
    return (
      <div>
        <div className="page-header">
          <h1 style={{fontSize: "6rem", marginTop: "3rem", marginBottom:'3rem', display: 'flex', justifyContent:'center'}}>Industries </h1>
        </div>
        <div style={{ display: 'flex'}} >
          <div id ='search' style={{ marginLeft:'8rem', float:'left'}}><Search searchCards={this.searchCards.bind(this)}/></div>
            <div style={{ marginLeft:'11rem', float:'left', width:'100%'}}>
              {this.state.filterChips}
            </div>
            <div style={{float:'right'}}>
              <Button variant="contained" color="default"
                onClick={this.handleClickOpen('paper')}
                style={{float:'right', marginLeft:'3rem', marginRight:'7rem', verticalAlign:'middle', fontSize:'13px'}} >
                Filter/Sort
                <FilterIcon/>
              </Button>
            </div>
            <Dialog
              open={this.state.open}
              onClose={this.handleClose}
              aria-labelledby="scroll-dialog-title"
            >
            <DialogContent>
            <Row style={{marginBottom:'.5rem'}}>
              <Col>
                <FormLabel style={{fontSize:'1.5rem'}}>Filter by:</FormLabel>
              </Col>
              <Col>
                <FormLabel style={{fontSize:'1.5rem', marginLeft:'10px'}}>Sort by:</FormLabel>
              </Col>
            </Row>
            <form style={{display: 'flex',flexWrap: 'wrap'}}>
            <Row>
              <Col style={{width:'380px', marginTop:'20px', marginLeft:'15px'}}>
              <div style={{fontSize:'10px', marginLeft:'-15px', marginBottom:'5px', marginTop:'-10px'}}>Salary range:</div>
              <Row>
                <div style={{marginTop:'-2px', fontSize:'10px', marginRight:'5px', width:'30px'}}>${this.state.filter.salary[0]}</div>
                <Range
                  style={{width:'160px', marginLeft:'8px', marginRight:'8px'}}
                  step={10000} min={0} max={200000} defaultValue={[0, 200000]} allowCross={false}
                  onChange={this.handleSalaryRangeChange} tipFormatter={value => value} />
                <div style={{marginTop:'-1px', fontSize:'10px', marginLeft:'10px'}}>${this.state.filter.salary[1]}</div>
              </Row>
              <div style={{fontSize:'10px', marginLeft:'-15px', marginBottom:'5px', marginTop:'20px'}}>Diversity score range:</div>
              <Row>
                <div style={{marginTop:'-2px', fontSize:'10px', marginRight:'5px', width:'30px'}}>{this.state.filter.divScore[0]}</div>
                <Range
                  style={{width:'160px', marginLeft:'8px', marginRight:'8px'}}
                  step={1} min={0} max={100} defaultValue={[0, 100]} allowCross={false}
                  onChange={this.handleDivScoreRangeChange} tipFormatter={value => value} />
                <div style={{marginTop:'-1px', fontSize:'10px', marginLeft:'10px'}}>{this.state.filter.divScore[1]}</div>
              </Row>
            </Col>
            <Col>
              <FormControl>
                <RadioGroup
                  value={this.state.filter.sort}
                  onChange={this.handleSortChange}
                  style={{margin:'${theme.spacing.unit}px 0'}}
                  >
                  <FormControlLabel value="name" control={<Radio color="primary"/>} label="Name alphabetically" />
                  <FormControlLabel value="salary" control={<Radio color="primary"/>} label="Salary" style={{marginTop:'-15px', minWidth: 165}}/>
                  <FormControlLabel value="divScore" control={<Radio color="primary"/>} label="Diversity Score" style={{marginTop:'-15px', minWidth: 165}}/>
                </RadioGroup>
                <FormControlLabel
                  control={
                    <Switch
                      checked={this.state.filter.desc}
                      onChange={this.handleSortOrderChange}
                      color="primary"
                    />
                  }
                  label="Descending order"
                />
              </FormControl>
            </Col>
          </Row>
        </form>
        </DialogContent>
          <DialogActions>
            <Button onClick={this.handleClose} color="primary">
              Cancel
            </Button>
            <Button id='applyFilter' onClick={this.handleApplyFilter.bind(this)} color="primary">
              Apply
            </Button>
          </DialogActions>
        </Dialog>
      </div>
      <div
        style={{display: 'flex', justifyContent:'center', alignItems:'center'}}>
      <PaginationGrid data={
        this.state.currentData.map((industry, index) => {
          if(industry) {
            return (
              <div key={index}>
              <IndustryCard image={industry.image}
                            name={industry.name}
                            avgSalary={industry.avg_salary}
                            divScore={parseFloat(industry.div_score).toFixed(1)}
                            topFields={industry.top_fields}
                            percentages={industry.percentages}
                            filter={this.state.searchFilter}/>
              </div>
            );
          } else {
            return;
          }
          })} searchFilter={this.state.searchFilter}
              pageCount = {this.state.pageCount}
              handlePageClick={this.handlePageClick.bind(this)}/>
        </div>
      </div>
    )
  }
}

export default Industries;
