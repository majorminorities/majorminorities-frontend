import React from 'react';
import anika from './images/anika.png';
import sheetal from './images/sheetal.jpg';
import jennie from './images/jennie.jpg';
import mark from './images/mark.jpg';
import jordan from './images/jordan.jpg';
import { Row, Col } from 'reactstrap';
import loading from'./images/loading.gif';

class About extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      data: null //This is what our data will eventually be loaded into
    };
  }

  componentDidMount() {
    this.loadData();
  }

  loadData() {
    let peopleJSON;
    const url = "http://api.majorminorities.me/git";

    fetch(url)
      .then(res => res.json())
      .then(data => peopleJSON = data)
      .then(contents => this.setState({
        data: contents
      }))
      .catch((error) => console.log("error " + error));
  }

  render() {
    console.log(this.state.data);

    if (!this.state.data) {
    return (
        <div>
          <h1 style={{display: 'flex', justifyContent:'center', alignItems:'center', marginTop: '10%'}}> Loading... </h1>
          <img src={loading} style={{display:'block', margin:'auto'}}/>
        </div>);
    }

    var mdata = {'c':0, 'i':0, 't':0};
    var jtdata = {'c':0, 'i':0, 't':0};
    var sdata = {'c':0, 'i':0, 't':0};
    var jdata = {'c':0, 'i':0, 't':0};
    var adata = {'c':0, 'i':0, 't':0};

    for(var i in this.state.data) {
      if (this.state.data[i].name == "Jordan Taylor") {
        jtdata['c'] = this.state.data[i].commits;
        jtdata['i'] = this.state.data[i].issues;
        jtdata['t'] = this.state.data[i].tests;
      } else if (this.state.data[i].name == "Mark Ramirez") {
        mdata['c'] = this.state.data[i].commits;
        mdata['i'] = this.state.data[i].issues;
        mdata['t'] = this.state.data[i].tests;
      } else if (this.state.data[i].name == "Jennie Kim") {
        jdata['c'] = this.state.data[i].commits;
        jdata['i'] = this.state.data[i].issues;
        jdata['t'] = this.state.data[i].tests;
      } else if (this.state.data[i].name == "Sheetal Poduri") {
        sdata['c'] = this.state.data[i].commits;
        sdata['i'] = this.state.data[i].issues;
        sdata['t'] = this.state.data[i].tests;
      } else if (this.state.data[i].name == "Anika Gera") {
        adata['c'] = this.state.data[i].commits;
        adata['i'] = this.state.data[i].issues;
        adata['t'] = this.state.data[i].tests;
      }
    }

    return (
      <main role="main" class="container">
        <h1 align="center">About Us</h1>
        <p align="center" style={{marginTop: 25}}>
          Our mission is to encourage an atmosphere of tolerance in the world around us and increase appreciation for inspiring figures from all walks of life.
        </p>
        <p align="center" style={{marginTop: 25}}>
          Major Minorities provides a platform to showcase the achievements of underrepresented minorities and celebrate their meaningful impact on our nation. We aim to inform and educate those who wish to understand how individuals of all kinds of cultures and backgrounds have uniquely influenced various fields of study. By highlighting the facts and figures of minority representation in different industries, we are better able to analytically recognize their significance. We hope to promote awareness of people’s distinctive and colorful backgrounds by advocating for diversity, equity, and inclusion of all types of minority groups.
        </p>
        <h1 align="center" style={{marginTop:50}}>Meet the team</h1>
          <Row id="profiles" style={{ width: '100%', display: 'flex', justifyContent:'center', alignItems:'center',  'margin-top':'40px', 'text-align': 'justify', '-ms-text-justify': 'distribute-all-lines', 'text-justify': 'distribute-all-lines'}}>
            <a id="anika" style = {{ 'vertical-align': 'top', 'text-align': 'center', 'align-items': 'center', 'orientation': 'vertical', 'display': 'inline-block', float: 'left',  padding: '0px 20px'}}>
              <div class="circular-portrait">
                <img src={anika} style={{height:'200px', width:'200px', objectFit: 'cover',
                  objectPosition: 'center -10px', 'border-radius': '50%', display:'inline-block'}}/>
              </div>
              <div class="bio" style={{marginTop: 25}}>
                <h5>Anika Gera</h5>
                <h6>Bio:</h6>
                <p style={{width: 300}}>Hi, I’m Anika. I’m a senior at UTCS from Dallas, TX. I love dancing and eating Chinese food!</p>
                <h6>Responsibility(s):</h6>
                <p style={{width: 300}}>I worked on the backend of the website.</p>
              </div>
            </a>
            <p></p>
            <a id="jennie" style = {{ 'vertical-align': 'top', 'text-align': 'center', 'align-items': 'center', 'orientation': 'vertical', 'display': 'inline-block', float: 'left',  padding: '0px 20px'}}>
              <div class="circular-portrait">
                <img src={jennie} style={{height:'200px', width:'200px', objectFit: 'cover',
                  objectPosition: 'center -27px', 'border-radius': '50%', display:'inline-block'}}/>
              </div>
              <div class="bio" style={{marginTop: 25}}>
                <h5>Jennie Kim</h5>
                <h6>Bio:</h6>
                <p style={{width: 300}}>I'm a senior at UTCS from Houston, Texas. I love hiking, photography, and binge watching TV.</p>
                <h6>Responsibility(s):</h6>
                <p style={{width: 300}}>I worked on the frontend of the website.</p>
              </div>
            </a>
            <p></p>
            <a id="sheetal" style = {{ 'vertical-align': 'top', 'text-align': 'center', 'align-items': 'center', 'orientation': 'vertical', 'display': 'inline-block', float: 'left',  padding: '0px 20px'}}>
              <div class="circular-portrait">
                <img src={sheetal} style={{height:'200px', width:'200px', objectFit: 'cover',
                  objectPosition: 'center -27px', 'border-radius': '50%', display:'inline-block'}}/>
              </div>
              <div class="bio" style={{marginTop: 25}}>
                <h5>Sheetal Poduri</h5>
                <h6>Bio:</h6>
                <p style={{width: 300}}>I’m a senior at UTCS from Plano, Texas. I love writing, hanging with friends, and singing (wildly off-key).</p>
                <h6>Responsibility(s):</h6>
                <p style={{width: 300}}>I did hosting and worked on the frontend of the website.</p>
              </div>
            </a>
            <span class="stretch" style={{width: '100%',  display: 'inline-block', 'font-size': 0, 'line-height': 0}}></span>
          </Row>
          <p></p>
          <Row id="profiles-second" style={{ display: 'flex', justifyContent:'center', alignItems:'center', width: '70%', 'margin-top':'30px', 'margin-left': 'auto', 'margin-right': 'auto', 'text-align': 'justify', '-ms-text-justify': 'distribute-all-lines', 'text-justify': 'distribute-all-lines' }}>
            <a id="mark" style = {{ 'vertical-align': 'top', 'horizontal-align': 'middle', 'text-align': 'center', 'orientation': 'vertical', 'display': 'inline-block', float: 'left',  padding: '0px 20px'}}>
              <div class="circular-portrait">
                <img src={mark} style={{height:'200px', width:'200px', objectFit: 'cover',
                  objectPosition: 'center -27px', 'border-radius': '50%', display:'inline-block'}}/>
              </div>
              <div class="bio" style={{marginTop: 25}}>
                <h5>Mark Ramirez</h5>
                <h6>Bio:</h6>
                <p style={{width: 300}}>I'm Mark. I'm a junior at UTCS and I'm from Brownsville, Texas. In my free time, I enjoy playing videogames, watching movies, and writing fiction.</p>
                <h6>Responsibility(s):</h6>
                <p style={{width: 300}}>I worked on the frontend of the website.</p>
              </div>
            </a>
            <p></p>
            <a id="jordan" style = {{ 'vertical-align': 'top', 'horizontal-align': 'middle',   'text-align': 'center', 'orientation': 'vertical', 'display': 'inline-block', float: 'left',  padding: '0px 20px'}}>
              <div class="circular-portrait">
                <img src={jordan} style={{height:'200px', width:'200px', objectFit: 'cover',
                  objectPosition: 'center -32px', 'border-radius': '50%', display:'inline-block'}}/>
              </div>
              <div class="bio" style={{marginTop: 25}}>
                <h5>Jordan Taylor</h5>
                <h6>Bio:</h6>
                <p style={{width: 300}}>I'm a junior at UTCS, from Houston, Texas. I'm passionate about playing the guitar, videogames, and of course, programming.</p>
                <h6>Responsibility(s):</h6>
                <p style={{width: 300}}>I worked on the backend of the website.</p>
              </div>
            </a>
            <span class="stretch"></span>
          </Row>

        <table style={{width:'100%', padding: '25px 50px 75px 100px', align:'center'}}>
          <h3 style={{marginTop:50}}>GitLab Statistics</h3>
          <tbody>
            <tr style={{'background-color': '#eee'}}>
              <th style={{ padding: '15px', 'text-align': 'left', 'border-spacing': '5px'}}>Name</th>
              <th style={{ padding: '15px', 'text-align': 'left', 'border-spacing': '5px'}}>Commits</th>
              <th style={{ padding: '15px', 'text-align': 'left', 'border-spacing': '5px'}}>Issues</th>
              <th style={{ padding: '15px', 'text-align': 'left', 'border-spacing': '5px'}}>Unit Tests</th>
            </tr>
            <tr>
              <td style={{ padding: '15px', 'text-align': 'left', 'border-spacing': '5px'}}>Jordan Taylor</td>
              <td style={{ padding: '15px', 'text-align': 'left', 'border-spacing': '5px'}}>{jtdata['c']}</td>
              <td style={{ padding: '15px', 'text-align': 'left', 'border-spacing': '5px'}}>{jtdata['i']}</td>
              <td style={{ padding: '15px', 'text-align': 'left', 'border-spacing': '5px'}}>{jtdata['t']}</td>
            </tr>
            <tr style={{'background-color': '#eee'}}>
              <td style={{ padding: '15px', 'text-align': 'left', 'border-spacing': '5px'}}>Mark Ramirez</td>
              <td style={{ padding: '15px', 'text-align': 'left', 'border-spacing': '5px'}}>{mdata['c']}</td>
              <td style={{ padding: '15px', 'text-align': 'left', 'border-spacing': '5px'}}>{mdata['i']}</td>
              <td style={{ padding: '15px', 'text-align': 'left', 'border-spacing': '5px'}}>{mdata['t']}</td>
            </tr>
            <tr>
              <td style={{ padding: '15px', 'text-align': 'left', 'border-spacing': '5px'}}>Anika Gera</td>
              <td style={{ padding: '15px', 'text-align': 'left', 'border-spacing': '5px'}}>{adata['c']}</td>
              <td style={{ padding: '15px', 'text-align': 'left', 'border-spacing': '5px'}}>{adata['i']}</td>
              <td style={{ padding: '15px', 'text-align': 'left', 'border-spacing': '5px'}}>{adata['t']}</td>
            </tr>
            <tr style={{'background-color': '#eee'}}>
              <td style={{ padding: '15px', 'text-align': 'left', 'border-spacing': '5px'}}>Sheetal Poduri</td>
              <td style={{ padding: '15px', 'text-align': 'left', 'border-spacing': '5px'}}>{sdata['c']}</td>
              <td style={{ padding: '15px', 'text-align': 'left', 'border-spacing': '5px'}}>{sdata['i']}</td>
              <td style={{ padding: '15px', 'text-align': 'left', 'border-spacing': '5px'}}>{sdata['t']}</td>
            </tr>
            <tr>
              <td style={{ padding: '15px', 'text-align': 'left', 'border-spacing': '5px'}}>Jennie Kim</td>
              <td style={{ padding: '15px', 'text-align': 'left', 'border-spacing': '5px'}}>{jdata['c']}</td>
              <td style={{ padding: '15px', 'text-align': 'left', 'border-spacing': '5px'}}>{jdata['i']}</td>
              <td style={{ padding: '15px', 'text-align': 'left', 'border-spacing': '5px'}}>{jdata['t']}</td>
            </tr>
            <tr style={{'background-color': '#eee'}}>
              <td style={{ padding: '15px', 'text-align': 'left', 'border-spacing': '5px'}}>Total</td>
              <td style={{ padding: '15px', 'text-align': 'left', 'border-spacing': '5px'}}>{jtdata['c'] + mdata['c'] + adata['c'] + sdata['c'] + jdata['c']}</td>
              <td style={{ padding: '15px', 'text-align': 'left', 'border-spacing': '5px'}}>{jtdata['i'] + mdata['i'] + adata['i'] + sdata['i'] + jdata['i']}</td>
              <td style={{ padding: '15px', 'text-align': 'left', 'border-spacing': '5px'}}>{jtdata['t'] + mdata['t'] + adata['t'] + sdata['t'] + jdata['t']}</td>
            </tr>
          </tbody>
        </table>
        <h3>Data</h3>
        <p>
          We collected our data from a variety of sources, including
          <a href="http://en.wikipedia.org/w/api.php"> Wikipedia</a>,
          <a href="https://www.census.gov/developers/"> Census.gov</a>, and
          <a href="https://www.wikidata.org/wiki/Wikidata:Data_access"> Wikidata</a>.
          All scraping was done in JavaScript, via the sites' RESTful APIs.
        </p>
        <h3>Tools</h3>
        <ul>
          <li><a href="http://getbootstrap.com/">Bootstrap</a>: used to design website</li>
          <li><a href="https://www.getpostman.com/">Postman</a>: used to design API</li>
          <li><a href="https://aws.amazon.com/s3/">Amazon S3</a>: used to host website</li>
          <li><a href="https://gitlab.com/">GitLab</a>: used for version control of source code</li>
          <li><a href="https://slack.com/">Slack</a>: used for communication within team</li>
          <li><a href="https://reactjs.org/">React</a>: used to dynamically render data on the website</li>
          <li><a href="http://flask.pocoo.org/">Flask</a>: used for database</li>
          <li><a href="https://www.mysql.com/">MySql</a>: used for database</li>
          <li><a href="https://d3js.org/">D3.js</a>used for data visualizations</li>
        </ul>
        <h3>Links</h3>
        <ul>
          <li>Link to frontend GitLab repo: <a href="https://gitlab.com/majorminorities/majorminorities-frontend">https://gitlab.com/majorminorities/majorminorities-frontend</a></li>
          <li>Link to backend GitLab repo: <a href="https://gitlab.com/majorminorities/majorminorities-backend">https://gitlab.com/majorminorities/majorminorities-backend</a></li>
          <li>Link to Postman API: <a href="https://documenter.getpostman.com/view/5451325/RWgjahhS">https://documenter.getpostman.com/view/5451325/RWgjahhS</a></li>
        </ul>
      </main>
    )
  }
}

export default About;
