import React from 'react';

class Search extends React.Component {

  handleSearch(event) {
    this.props.searchCards(event.target.value);
  }

  render() {
    return (
      <div className="form-group form-inline">
        <div className="input-field">
          <div class="col-md-1"><label>Search</label></div>
          <div class="col-md-1"></div>
          <div class="col-md-1"><input type="text" onKeyUp={this.handleSearch.bind(this)}/></div>
        </div>
      </div>
    )
  }
}

export default Search;
