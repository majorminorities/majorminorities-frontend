import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Card, {
  } from "@material/react-card";
import '@material/react-card/dist/card.css';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import CardMedia from '@material-ui/core/CardMedia';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Highlighter from "react-highlight-words";

class MinorityCard extends Component {

  render() {

    const textStyle = {
      marginTop:'1rem',
      textIndent: '-6px'
    }

    const reduceBulletTextGap = {

    }

    var minority = this.props;
    console.log(this.props);
        return (
          <div>
          <Card style={{ width: '280px', margin:'5rem'}}>
            <CardMedia component="img"
                       image={minority.image}
                       style={{height: '22rem',width: '280px', objectFit: 'cover'}}/>
            <CardContent style={{height: '22.5rem'}}>
              <Typography gutterBottom variant="display2" style={{fontSize:'18px', textIndent: '-6px', color:"#000000"}}>
              <Highlighter
                  highlightClassName="HighlightClass"
                  searchWords={minority.filter}
                  autoEscape={true}
                  textToHighlight={minority.name}
                />
              </Typography>
              <Typography component="p" style={{fontSize:"12.5px", height:'9rem'}}>
                <li style={textStyle}><span style={reduceBulletTextGap}>Percent of US:  <Highlighter
                  highlightClassName="HighlightClass"
                  searchWords={minority.filter}
                  autoEscape={true}
                  textToHighlight={"" + minority.uspercent}
                />% </span></li>
                <li style={textStyle}><span style={reduceBulletTextGap}>Representation in {Object.keys(minority.percentages)[0]}: <Highlighter
                  highlightClassName="HighlightClass"
                  searchWords={minority.filter}
                  autoEscape={true}
                  textToHighlight={"" + minority.percentages[Object.keys(minority.percentages)[0]]}
                />% </span></li>
                <li style={textStyle}><span style={reduceBulletTextGap}>Representation in {Object.keys(minority.percentages)[1]}: <Highlighter
                  highlightClassName="HighlightClass"
                  searchWords={minority.filter}
                  autoEscape={true}
                  textToHighlight={"" + minority.percentages[Object.keys(minority.percentages)[1]]}
                />% </span></li>
                <li style={textStyle}><span style={reduceBulletTextGap}>Representation in {Object.keys(minority.percentages)[2]}: <Highlighter
                  highlightClassName="HighlightClass"
                  searchWords={minority.filter}
                  autoEscape={true}
                  textToHighlight={"" + minority.percentages[Object.keys(minority.percentages)[2]]}
                />% </span></li>
                <li style={textStyle}><span style={reduceBulletTextGap}>Representation in {Object.keys(minority.percentages)[3]}: <Highlighter
                  highlightClassName="HighlightClass"
                  searchWords={minority.filter}
                  autoEscape={true}
                  textToHighlight={"" + minority.percentages[Object.keys(minority.percentages)[3]]}
                />% </span></li>
              </Typography>
            </CardContent>
            <CardActions>
              <Link to={'/minority/' + minority.name} style={{textDecoration:'none'}}>
                <Button size="small" color="primary" style={{fontSize:"1.3rem", marginLeft:"5", marginBottom:"5"}}>
                  Learn More
                </Button>
              </Link>
            </CardActions>
          </Card>
          </div>
        );
    }
};

export default MinorityCard;
