import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Card, {
  } from "@material/react-card";
import '@material/react-card/dist/card.css';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import CardMedia from '@material-ui/core/CardMedia';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Highlighter from "react-highlight-words";

class PersonCard extends Component {

  concatenateMinorities(minority1, minority2) {
    var temp;
    if (minority2==='None') {
      return minority1;
    }
    if (minority2!== "None") {
      if (minority1 > minority2) {
        temp=minority1;
        minority1=minority2;
        minority2=temp;
      }
      var string = minority1;
      string += "/";
      string += minority2;
    }
    return string;
  }

  render() {
    const textStyle = {
      marginTop:'3px',
      textIndent: '-6px',
      fontSize:'12px'
    }

    const reduceBulletTextGap = {

    }

    const buttonStyle = {
      fontSize:"1.3rem",
      marginLeft:"5",
      marginBottom:"5",
    }

    const imageStyle = {
      height: '31rem',
      width: '280px',
      objectFit: 'cover',
      objectPosition: '50% 5%'
    }

    var person = this.props;
    return (
      <div>
        <Card style={{ width: '280px', margin:'4rem'}}>
          <CardMedia component="img"
                    image={person.image}
                    style={imageStyle}/>
          <CardContent style={{height: '17rem'}}>
            <Typography  variant="display1" style={{fontSize:'18px', textIndent: '-6px', color:"#000000"}}>
              <Highlighter
                    highlightClassName="HighlightClass"
                    searchWords={person.filter}
                    autoEscape={true}
                    textToHighlight={person.name}
              />
            </Typography>
            <Typography component="p" style={{fontSize:"12.5px", height:'9rem'}}>
              <li style={textStyle}><span style={reduceBulletTextGap}>Date of Birth: <Highlighter
                    highlightClassName="HighlightClass"
                    searchWords={person.filter}
                    autoEscape={true}
                    textToHighlight={person.birth_date}
              /> </span></li>
              <li style={textStyle}><span style={reduceBulletTextGap}>Place of Birth: <Highlighter
                    highlightClassName="HighlightClass"
                    searchWords={person.filter}
                    autoEscape={true}
                    textToHighlight={person.birth_place}
              /> </span></li>
              <li style={textStyle}><span style={reduceBulletTextGap}>Minority: <Highlighter
                    highlightClassName="HighlightClass"
                    searchWords={person.filter}
                    autoEscape={true}
                    textToHighlight={this.concatenateMinorities(person.minority_1, person.minority_2)}
              /> </span></li>
              <li style={textStyle}><span style={reduceBulletTextGap}>Industry: <Highlighter
                    highlightClassName="HighlightClass"
                    searchWords={person.filter}
                    autoEscape={true}
                    textToHighlight={person.industry}
              /> </span></li>
              <li style={textStyle}><span style={reduceBulletTextGap}>Occupation: <Highlighter
                    highlightClassName="HighlightClass"
                    searchWords={person.filter}
                    autoEscape={true}
                    textToHighlight={person.occupation}
              /> </span></li>
            </Typography>
          </CardContent>
          <CardActions>
            <Link to={'/person/' + person.name} style={{textDecoration:'none'}}>
              <Button size="small" color="primary" style={buttonStyle}>
                Learn More
              </Button>
            </Link>
          </CardActions>
        </Card>
      </div>
      );
    }
};

export default PersonCard;
