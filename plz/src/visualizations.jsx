import React from 'react';
import loading from'./images/loading.gif';
import $ from 'jquery';
import 'rc-slider/assets/index.css';
import 'rc-tooltip/assets/bootstrap.css';
import * as d3 from "d3";

const Slider = require('rc-slider');
const createSliderWithTooltip = Slider.createSliderWithTooltip;

class Visualizations extends React.Component {
  constructor() {
    super();

    this.state = {
      pData: null,
      iData: null,
      mData: null,
      devSData: null,
      devNData:null,
      open: false
    };
  }

  handlePageClick = (data) => {
    let selected = data.selected;
    let offset = Math.ceil(selected * 9);

    this.setState({offset: offset}, () => {
      this.updateShownIndustries();
    });
  };

  componentDidMount() {
    this.loadData();
    this.loadDevData();
  }

  componentDidUpdate() {
    this.createChart1();
    this.createChart2();
    this.createChart3();
    this.createChart4();
    this.createChart5();
    this.createChart6();
  }

  loadData() {
    const pUrl = "http://api.majorminorities.me/person";
    const iUrl = "http://api.majorminorities.me/industry";
    const mUrl = "http://api.majorminorities.me/minority";
    $.ajax({
      url      : pUrl,
      data     : {limit: 100, offset: 0},
      dataType : 'json',
      type     : 'GET',

      success: data => {
        this.setState({pData:data})
      },
      error: (xhr, status, err) => {
        console.error(this.props.url, status, err.toString());
    }});

    $.ajax({
      url      : iUrl,
      data     : {limit: 100, offset: 0},
      dataType : 'json',
      type     : 'GET',

      success: data => {
        this.setState({iData:data})
      },
      error: (xhr, status, err) => {
        console.error(this.props.url, status, err.toString());
    }});

    $.ajax({
      url      : mUrl,
      data     : {limit: 100, offset: 0},
      dataType : 'json',
      type     : 'GET',

      success: data => {
        this.setState({mData:data})
      },
      error: (xhr, status, err) => {
        console.error(this.props.url, status, err.toString());
    }});
  }

  loadDevData() {
  const statesUrl = "https://api.relievepoverty.me/v1/states";
  const newsUrl = "https://api.relievepoverty.me/v1/news";

  $.ajax({
    url      : statesUrl,
    data     : {limit: 100, offset: 0},
    dataType : 'json',
    type     : 'GET',

    success: data => {

      this.setState({devSData:data})
    },
    error: (xhr, status, err) => {
      console.error(this.props.url, status, err.toString());
    }});

    $.ajax({
      url      : newsUrl,
      data     : {limit: 500, offset: 0},
      dataType : 'json',
      type     : 'GET',

      success: data => {

        this.setState({devNData:data})
      },
      error: (xhr, status, err) => {
        console.error(this.props.url, status, err.toString());
      }});

  }
  handleClickOpen = scroll => () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  createChart1() {
    /*
      This method is going to be a bar graph mapping industries to their average salaries
      y axis - money
      x axis - industry
    */
    var yOffset = 200
    if (!this.state.iData)
      return;

    //console.log(this.state.iData)
    var industryNames = [];
    this.state.iData.forEach(function(item) {
      industryNames.push(item.name)
      //console.log(item.name)
    });

    const node = this.node;

    const yScale = d3.scaleLinear()
      .domain([0, d3.max(this.state.iData, (d) =>  d.avg_salary)])
      .range([0, 200])
    var yAxis = d3.axisLeft(yScale)

    const xScale = d3.scaleOrdinal()
      .domain(industryNames)
      .range([100, 500])

    d3.select(node)
      .select('#chart1')
      .selectAll('rect')
      .data(this.state.iData)
      .enter()
      .append('rect')

    d3.select(node)
      .select('#chart1')
      .selectAll('rect')
      .data(this.state.iData)
      .exit()
      .remove()

    var color = d3.scaleOrdinal(d3.schemeCategory10);
    d3.select(node)
      .select('#chart1')
      .selectAll('rect')
      .data(this.state.iData)
      .style('fill', (d) => color(d.name))
      .attr('x', (d, i) => 50 + (500-100)/11*i)
      .attr('y', (d) => yOffset + 50)//500-yScale(d.avg_salary))
      .attr('width',  (500-150)/11)
      .attr('height', function(d) {
        return yScale(d.avg_salary)
      })

    d3.select(node)
      .append('g')
      .attr('class', 'y axis')
      .attr('transform', 'translate(45, ' + (yOffset + 50) + ')')
      .call(yAxis)

    d3.select(node)
      .append('g')
      .attr('class', 'legend')
      .selectAll('text')
      .data(this.state.iData)
      .enter()
      .append('text')
      .text((d) => "• " + d.name)
      .attr('fill', (d) => color(d.name))
      .attr('y', (d, i) => 25 * (i + 1) + 235)
      .attr('x', () => 460)

    d3.select(node)
      .append('text')
      .attr('x', 700/2)
      .attr('y', 25 + yOffset)
      .attr('text-anchor', 'middle')
      .style('font-size', '16px')
      .style('text-decoration', 'underline')
      .style('font-weight', 'bold')
      .text('Average Salary Per Industry')

    d3.select(node)
      .append('text')
      .attr('x', 700/2)
      .attr('y', yOffset - 30)
      .attr('text-anchor', 'middle')
      .style('font-size', '20px')
      .style('text-decoration', 'underline')
      .style('font-weight', 'bold')
      .text('Our Visualizations')
  }

  createChart2() {
    var yOffset = 400;
    /*
      This method is going to be a bar graph mapping industries to their industry div score
      y axis - industry
      x axis - score
    */

    if (!this.state.iData)
      return;

    var myData = this.state.iData.slice();
    var industryNames = [];
    this.state.iData.forEach(function(item) {
      industryNames.push(item.name)
      //console.log(item.name)
    });

    const node = this.node;

    const yScale = d3.scaleLinear()
      .domain([0, 100])//d3.max(myData, (d) =>  d.div_score)])
      .range([0, 200])
    var yAxis = d3.axisLeft(yScale)

    const xScale = d3.scaleOrdinal()
      .domain(industryNames)
      .range([100, 500])

    d3.select(node)
      .select('#chart2')
      .selectAll('rect')
      .data(myData)
      .enter()
      .append('rect')

    d3.select(node)
      .select('#chart2')
      .selectAll('rect')
      .data(myData)
      .exit()
      .remove()

    var color = d3.scaleOrdinal(d3.schemeCategory10);
    d3.select(node)
      .select('#chart2')
      .selectAll('rect')
      .data(myData)
      .style('fill', (d) => color(d.name))
      .attr('x', (d, i) => 50 + (500-100)/11*i)
      .attr('y', () => 300 + yOffset)//500-yScale(d.avg_salary))
      .attr('width',  (500-150)/11)
      .attr('height', function(d) {
        return yScale(d.div_score)
      })

    d3.select(node)
      .append('g')
      .attr('class', 'y axis')
      .attr('transform', 'translate(45,' + (300+yOffset) + ')')
      .call(yAxis)

    d3.select(node)
      .append('g')
      .attr('class', 'legend')
      .selectAll('text')
      .data(myData)
      .enter()
      .append('text')
      .text((d) => "• " + d.name)
      .attr('fill', (d) => color(d.name))
      .attr('y', (d, i) => 25 * (i + 1) + 285 + yOffset)
      .attr('x', () => 460)

    d3.select(node)
      .append('text')
      .attr('x', 700/2)
      .attr('y', 270 + yOffset)
      .attr('text-anchor', 'middle')
      .style('font-size', '16px')
      .style('text-decoration', 'underline')
      .style('font-weight', 'bold')
      .text('Diversity Score Per Industry')

  }

  createChart3() {
    var yOffset = 800;
    /*
      This method is going to be a bar graph mapping industries to their industry div score
      y axis - industry
      x axis - score
    */

    if (!this.state.mData)
      return;

    var myData = this.state.mData.slice();
    var minorityNames = [];
    this.state.mData.forEach(function(item) {
      minorityNames.push(item.name)
      //console.log(item.name)
    });

    const node = this.node;

    const yScale = d3.scaleLinear()
      .domain([0, 100])//d3.max(myData, (d) =>  d.div_score)])
      .range([0, 200])
    var yAxis = d3.axisLeft(yScale);

    const xScale = d3.scaleOrdinal()
      .domain(minorityNames)
      .range([100, 500])

    d3.select(node)
      .select('#chart3')
      .selectAll('rect')
      .data(myData)
      .enter()
      .append('rect')

    d3.select(node)
      .select('#chart3')
      .selectAll('rect')
      .data(myData)
      .exit()
      .remove()

    var color = d3.scaleOrdinal(d3.schemeCategory10);
    d3.select(node)
      .select('#chart3')
      .selectAll('rect')
      .data(myData)
      .style('fill', (d) => color(d.name))
      .attr('x', (d, i) => 50 + (500-100)/11*i)
      .attr('y', () => 300 + yOffset)//500-yScale(d.avg_salary))
      .attr('width',  (500-150)/11)
      .attr('height', function(d) {
        return yScale(d.percentage_us)
      })

    d3.select(node)
      .append('g')
      .attr('class', 'y axis')
      .attr('transform', 'translate(45,' + (300+yOffset) + ')')
      .call(yAxis)

    d3.select(node)
      .append('g')
      .attr('class', 'legend')
      .selectAll('text')
      .data(myData)
      .enter()
      .append('text')
      .text((d) => "• " + d.name)
      .attr('fill', (d) => color(d.name))
      .attr('y', (d, i) => 25 * (i + 1) + 285 + yOffset)
      .attr('x', () => 460)

    d3.select(node)
      .append('text')
      .attr('x', 700/2)
      .attr('y', 270 + yOffset)
      .attr('text-anchor', 'middle')
      .style('font-size', '16px')
      .style('text-decoration', 'underline')
      .style('font-weight', 'bold')
      .text('Known Minority Percentages in the Workforce')
  }

  createChart4() {
    var yOffset = 1200
    /*
      This method is going to be a bar graph mapping industries to their industry div score
      y axis - industry
      x axis - score
    */

    if (!this.state.devSData)
      return;

    var myData = this.state.devSData.data
    //var minorityNames = []

    const node = this.node

    const yScale = d3.scaleLinear()
      .domain([0, 50])//d3.max(myData, (d) =>  d.div_score)])
      .range([0, 200])
    var yAxis = d3.axisLeft(yScale)

    const xScale = d3.scaleOrdinal()
      .domain((d)=>d.name)
      .range([0, 800])
    var xAxis = d3.axisTop(xScale)
      .tickFormat((d) => d.name)

    d3.select(node)
      .select('#chart4')
      .selectAll('rect')
      .data(myData)
      .enter()
      .append('rect')

    d3.select(node)
      .select('#chart4')
      .selectAll('rect')
      .data(myData)
      .exit()
      .remove()

    var color = d3.scaleOrdinal(d3.schemeCategory10)
    d3.select(node)
      .select('#chart4')
      .selectAll('rect')
      .data(myData)
      .style('fill', (d, i) => color(d.name))
      .attr('x', (d, i) => 50 + (800)/50*i)
      .attr('y', (d) => 350 + yOffset)//500-yScale(d.avg_salary))
      .attr('width',  (800)/50)
      .attr('height', function(d, i) {
        return yScale(d.child_poverty_rate)
      })

    d3.select(node)
      .select('#chart4')
      .append('g')
      .selectAll('text')
      .data(myData)
      .enter()
      .append('text')
      .text((d) => d.name)
      .style('font-size', '10px')
      .attr('text-anchor', 'left')
      .attr('transform', 'rotate(-90)')
      .attr('fill', 'black')
      .attr('y', (d, i) => 60 + (800/50*i) )
      .attr('x', (d, i) => yOffset *-1 - 345)

    d3.select(node)
      .append('g')
      .attr('class', 'y axis')
      .attr('transform', 'translate(45,' + (350+yOffset) + ')')
      .call(yAxis)
/*
    d3.select(node)
      .append('g')
      .attr('class', 'legend')
      .selectAll('text')
      .data(myData)
      .enter()
      .append('text')
      .text((d) => "• " + d.name)
      .attr('fill', (d) => color(d.name))
      .attr('y', (d, i) => 25 * (i + 1) + 285 + yOffset)
      .attr('x', (d, i) => 460)
*/
    d3.select(node)
      .append('text')
      .attr('x', 900/2)
      .attr('y', 270 + yOffset - 30)
      .attr('text-anchor', 'middle')
      .style('font-size', '20px')
      .style('text-decoration', 'underline')
      .style('font-weight', 'bold')
      .text('Provider Visualizations')

    d3.select(node)
      .append('text')
      .attr('x', 900/2)
      .attr('y', 270 + yOffset)
      .attr('text-anchor', 'middle')
      .style('font-size', '16px')
      .style('text-decoration', 'underline')
      .style('font-weight', 'bold')
      .text('Child Poverty Rate Percentage by State')
  }

  createChart5() {
    var yOffset = 1600
    /*
      This method is going to be a bar graph mapping industries to their industry div score
      y axis - industry
      x axis - score
    */

    if (!this.state.devSData)
      return;

    var myData = this.state.devSData.data
    //var minorityNames = []

    const node = this.node

    const yScale = d3.scaleLinear()
      .domain([0, 50])//d3.max(myData, (d) =>  d.div_score)])
      .range([0, 200])
    var yAxis = d3.axisLeft(yScale)

    const xScale = d3.scaleOrdinal()
      .domain((d)=>d.name)
      .range([0, 900])
    var xAxis = d3.axisTop(xScale)
      .tickFormat((d) => d.name)

    d3.select(node)
      .select('#chart5')
      .selectAll('rect')
      .data(myData)
      .enter()
      .append('rect')

    d3.select(node)
      .select('#chart5')
      .selectAll('rect')
      .data(myData)
      .exit()
      .remove()

    var color = d3.scaleOrdinal(d3.schemeCategory10)
    d3.select(node)
      .select('#chart5')
      .selectAll('rect')
      .data(myData)
      .style('fill', (d, i) => color(d.name))
      .attr('x', (d, i) => 50 + (800)/50*i)
      .attr('y', (d) => 350 + yOffset)//500-yScale(d.avg_salary))
      .attr('width',  (800)/50)
      .attr('height', function(d, i) {
        return yScale(d.below_poverty_rate)
      })

    d3.select(node)
      .select('#chart5')
      .append('g')
      .selectAll('text')
      .data(myData)
      .enter()
      .append('text')
      .text((d) => d.name)
      .style('font-size', '10px')
      .attr('text-anchor', 'left')
      .attr('transform', 'rotate(-90)')
      .attr('fill', 'black')
      .attr('y', (d, i) => 60 + (800/50*i) )
      .attr('x', (d, i) => yOffset *-1 - 345)

    d3.select(node)
      .append('g')
      .attr('class', 'y axis')
      .attr('transform', 'translate(45,' + (350+yOffset) + ')')
      .call(yAxis)
/*
    d3.select(node)
      .append('g')
      .attr('class', 'legend')
      .selectAll('text')
      .data(myData)
      .enter()
      .append('text')
      .text((d) => "• " + d.name)
      .attr('fill', (d) => color(d.name))
      .attr('y', (d, i) => 25 * (i + 1) + 285 + yOffset)
      .attr('x', (d, i) => 460)
*/

    d3.select(node)
      .append('text')
      .attr('x', 900/2)
      .attr('y', 270 + yOffset)
      .attr('text-anchor', 'middle')
      .style('font-size', '16px')
      .style('text-decoration', 'underline')
      .style('font-weight', 'bold')
      .text('Below Poverty Rate Percentage by State')
  }

  createChart6() {
    var yOffset = 2000
    /*
      This method is going to be a bar graph mapping industries to their industry div score
      y axis - industry
      x axis - score
    */

    if (!this.state.devNData)
      return;

      var rawData = {};
      this.state.devNData.data.forEach(function(article) {
        rawData[article.source] = (rawData[article.source] || 0) + 1
      });

      var data = []
      for(var article in rawData) {
        if(rawData.hasOwnProperty(article)) {
          data.push({site: article, freq:rawData[article]})
        }
      }

      console.log(data)


    var myData = data
    //var minorityNames = []

    const node = this.node

    const yScale = d3.scaleLinear()
      .domain([0, 35])//d3.max(myData, (d) =>  d.div_score)])
      .range([0, 300])
    var yAxis = d3.axisLeft(yScale)

    const xScale = d3.scaleOrdinal()
      .domain((d)=>d.site)
      .range([0, 900])

    d3.select(node)
      .select('#chart6')
      .selectAll('rect')
      .data(myData)
      .enter()
      .append('rect')

    d3.select(node)
      .select('#chart6')
      .selectAll('rect')
      .data(myData)
      .exit()
      .remove()

    var color = d3.scaleOrdinal(d3.schemeCategory10)
    d3.select(node)
      .select('#chart6')
      .selectAll('rect')
      .data(myData)
      .style('fill', (d, i) => color(d.site))
      .attr('x', (d, i) => 50 + (900)/92*i)
      .attr('y', (d) => 350 + yOffset + 60)//500-yScale(d.avg_salary))
      .attr('width',  (900)/92)
      .attr('height', function(d, i) {
        return yScale(d.freq)
      })

    d3.select(node)
      .select('#chart6')
      .append('g')
      .selectAll('text')
      .data(myData)
      .enter()
      .append('text')
      .text((d) => d.site)
      .style('font-size', '10px')
      .attr('text-anchor', 'left')
      .attr('transform', 'rotate(-90)')
      .attr('fill', 'black')
      .attr('y', (d, i) => 58 + (900/92*i) )
      .attr('x', (d, i) => yOffset *-1 - 405)

    d3.select(node)
      .append('g')
      .attr('class', 'y axis')
      .attr('transform', 'translate(45,' + (350+60+yOffset) + ')')
      .call(yAxis)

    d3.select(node)
      .append('text')
      .attr('x', 900/2)
      .attr('y', 270 + yOffset)
      .attr('text-anchor', 'middle')
      .style('font-size', '16px')
      .style('text-decoration', 'underline')
      .style('font-weight', 'bold')
      .text('Number of News Articles Per Source')
  }

  render() {
    if (!this.state.pData || !this.state.mData || !this.state.iData) {
      return (
        <div>
          <h1 style={{display: 'flex', justifyContent:'center', alignItems:'center', marginTop: '10%'}}> Loading... </h1>
          <img src={loading} style={{display:'block', margin:'auto'}}/>
        </div>);
    }
    return (
    <div id='main_viz_div' style={{display: 'flex', justifyContent:'center', alignItems:'center'}}>
      <svg ref = {node => this.node = node} width = {900} height= {3000}>
        <g id='chart1'></g>
        <g id='chart2'></g>
        <g id='chart3'></g>
        <g id='chart4'></g>
        <g id='chart5'></g>
        <g id='chart6'></g>
        <g id='chart7'></g>
        <g id='chart8'></g>
      </svg>
    </div>
    )
  }
}

export default Visualizations;
