import React from 'react';
import { Row, Col } from 'reactstrap';
import PaginationGrid from './paginationGrid.jsx';
import MinorityCard from './minorityCard.jsx';
import loading from'./images/loading.gif';
import $ from 'jquery';
import Chip from '@material-ui/core/Chip';
import Button from '@material-ui/core/Button';
import FilterIcon from '@material-ui/icons/FilterList';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import Search from './Search';
import 'rc-slider/assets/index.css';
import 'rc-tooltip/assets/bootstrap.css';

const Slider = require('rc-slider');
const createSliderWithTooltip = Slider.createSliderWithTooltip;
const Range = createSliderWithTooltip(Slider.Range);

class Minorities extends React.Component {
  constructor() {
    super();

    this.state = {
      data: null,
      filteredData:null,
      currentData: null,
      pageCount: null,
      searchFilter:[],
      offset: 0,
      filter: {percentUS:[0, 100], sort:'name', desc:false},
      filterChips:[],
      open: false,
    };
  }

  handlePageClick = (data) => {
    let selected = data.selected;
    let offset = Math.ceil(selected * 9);

    this.setState({offset: offset}, () => {
      this.updateShownMinorities();
    });
  };

  updateShownMinorities() {
    var pageData = []
    var x;
    try {
      for ( x = this.state.offset; x < (this.state.offset + 9); x++) {
        if (this.state.filteredData[x]) {
          pageData.push(this.state.filteredData[x]);
        }
      }
    }
    catch(exception) {}
    this.setState({currentData: pageData, pageCount: Math.ceil(this.state.filteredData.length / 9)}, this.updateFilterChips);
  }

  updateFilterChips() {
    var chips = [];
    if (this.state.filter.percentUS[0] > 0 || this.state.filter.percentUS[1] < 100) {
      var salaryLabel = "Percent of US: " + this.state.filter.percentUS[0] + '%-' + this.state.filter.percentUS[1] + '%';
      chips.push(<Chip key='percentUS'
                       style={{marginRight: '1rem', marginBottom: '1rem'}}
                       label={salaryLabel}
                       onDelete={this.deletePercentUSRange} />);
    }
    this.setState({filterChips: chips});
  }

  deletePercentUSRange = () => {
    var chips = this.state.filterChips;
    for (var x = 0; x < chips.length; x++) {
      if (chips[x].key === 'percentUS') {
        chips.splice(x,1);
      }
    }
    this.setState({
      filter: {percentUS:[0,100], sort: this.state.filter.sort, desc:this.state.filter.desc},
      filterChips: chips}
    , this.handleApplyFilter.bind(this));
  };

  componentDidMount() {
    this.loadData();
  }

  loadData() {
    const url = "http://api.majorminorities.me/minority";
    $.ajax({
      url      : url,
      data     : {limit: 9, offset: this.state.offset},
      dataType : 'json',
      type     : 'GET',

      success: data => {
        var dataSorted = this.sortData(data);
        var pageData = []
        var x;
        try {
          for (x = this.state.offset; x < (this.state.offset + 9); x++) {
            pageData.push(dataSorted[x]);
          }
        }
        catch(exception) {}
        this.setState({
          data: dataSorted,
          filteredData:dataSorted,
          currentData:pageData,
          searchFilter:[],
          pageCount: Math.ceil(data.length / 9)
        });
      },

      error: (xhr, status, err) => {
        console.error(this.props.url, status, err.toString());
      }});
  }

  searchCards(query) {
    let minorities = this.state.data.filter((minority) => {
      return minority.name.toLowerCase().includes(query) ||
        minority.percentage_us.toString().includes(query)
    });
    this.setState({filteredData:minorities, searchFilter:[query], filter: {percentUS:[0, 100], sort:'name', desc:false}}, this.updateShownMinorities);
  }

  handleClickOpen = () => () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  sortData(data) {
    var newData=data;
    switch(this.state.filter.sort) {
      case "name":
        newData.sort(function(a, b) {
          if(a.name < b.name) { return -1; }
          if(a.name > b.name) { return 1; }
          return 0;
        })
        break;
      case "percentUS":
        newData.sort(function(a, b) {
          if(a.percentage_us < b.percentage_us) { return -1; }
          if(a.percentage_us > b.percentage_us) { return 1; }
          return 0;
        })
        break;
      default:
    }

    if(this.state.filter.desc) {
      return newData.reverse();
    } else {
      return newData;
    }
  }

  handleApplyFilter = () => {
    var newData = [];
    this.handleClose();
    newData = this.filterPercentUS();
    newData = this.sortData(newData);
    this.setState({filteredData: newData, offset:0}, this.updateShownMinorities);
  };

  filterPercentUS() {
    var newData = [];
    var x;
    for (x = 0; x < this.state.data.length; x++){
      if (this.state.data[x].percentage_us >= this.state.filter.percentUS[0] &&
         this.state.data[x].percentage_us <= this.state.filter.percentUS[1] ) {
        newData.push(this.state.data[x]);
      }
    }
    return newData;
  }

  handleSortChange = event => {
    this.setState({ filter: {percentUS:this.state.filter.percentUS, sort: event.target.value, desc:this.state.filter.desc} });
  };

  handleSortOrderChange = event => {
    this.setState({ filter:  {percentUS:this.state.filter.percentUS, sort: this.state.filter.sort, desc:event.target.checked}});
  };

  handlepercentUSRangeChange = value => {
    this.setState({ filter: {percentUS:value, divScore:value, sort: this.state.filter.sort, desc:this.state.filter.desc} });
  };

  render() {
    if (!this.state.data) {
      return (
        <div >
          <h1 style={{display: 'flex', justifyContent:'center', alignItems:'center', marginTop: '10%'}}> Loading... </h1>
          <img src={loading} style={{display:'block', margin:'auto'}}/>
        </div>
      );
    }
    return (
      <div>
      <div className="page-header">
        <h1 style={{fontSize: "6rem", marginTop: "3rem", marginBottom:'3rem', display: 'flex', justifyContent:'center'}}>Minorities </h1>
      </div>
      <div style={{ display: 'flex'}} >
        <div id ='search' style={{ marginLeft:'10rem', float:'left'}}><Search searchCards={this.searchCards.bind(this)}/></div>
        <div style={{ marginLeft:'11rem', float:'left', width:'100%'}}>
          {this.state.filterChips}
        </div>
        <div style={{float:'right'}}>
          <Button variant="contained" color="default"
            onClick={this.handleClickOpen('paper')}
            style={{float:'right', marginLeft:'3rem', marginRight:'9rem', verticalAlign:'middle', fontSize:'13px'}} >
            Filter/Sort
            <FilterIcon/>
          </Button>
        </div>
        <Dialog
          open={this.state.open}
          onClose={this.handleClose}
          aria-labelledby="scroll-dialog-title"
        >
        <DialogContent>
          <Row style={{marginBottom:'.5rem'}}>
            <Col>
              <FormLabel style={{fontSize:'1.5rem'}}>Filter by:</FormLabel>
            </Col>
            <Col>
              <FormLabel style={{fontSize:'1.5rem', marginLeft:'10px'}}>Sort by:</FormLabel>
            </Col>
          </Row>
          <form style={{display: 'flex',flexWrap: 'wrap'}}>
            <Row>
              <Col style={{width:'380px', marginTop:'20px', marginLeft:'15px'}}>
                <div style={{fontSize:'10px', marginLeft:'-15px', marginBottom:'5px', marginTop:'-10px'}}>Percentage of US:</div>
                <Row>
                  <div style={{marginTop:'-2px', fontSize:'10px', marginRight:'5px', width:'30px'}}>{this.state.filter.percentUS[0]}%</div>
                  <Range
                    style={{width:'160px', marginLeft:'8px', marginRight:'8px'}}
                    step={1} min={0} max={100} defaultValue={[this.state.filter.percentUS[0], this.state.filter.percentUS[1]]} allowCross={false}
                    onChange={this.handlepercentUSRangeChange} tipFormatter={value => value} />
                  <div style={{marginTop:'-1px', fontSize:'10px', marginLeft:'10px'}}>{this.state.filter.percentUS[1]}%</div>
                </Row>
              </Col>
              <Col>
                <FormControl>
                  <RadioGroup
                    value={this.state.filter.sort}
                    onChange={this.handleSortChange}
                    style={{margin:'${theme.spacing.unit}px 0'}}>
                    <FormControlLabel value="name" control={<Radio color="primary"/>} label="Name alphabetically" />
                    <FormControlLabel value="percentUS" control={<Radio color="primary"/>} label="Percentage of US" style={{marginTop:'-15px', minWidth: 165}}/>
                  </RadioGroup>
                  <FormControlLabel
                    control={
                      <Switch
                        checked={this.state.filter.desc}
                        onChange={this.handleSortOrderChange}
                        color="primary"/>
                    }
                    label="Descending order"/>
                </FormControl>
              </Col>
            </Row>
          </form>
        </DialogContent>
        <DialogActions>
          <Button onClick={this.handleClose} color="primary">
              Cancel
          </Button>
          <Button id='applyFilter' onClick={this.handleApplyFilter.bind(this)} color="primary">
            Apply
          </Button>
        </DialogActions>
      </Dialog>
    </div>
    <div id='minoritiesGrid' style={{display: 'flex', justifyContent:'center', alignItems:'center'}}>
      <PaginationGrid data={
        this.state.currentData.map((minority, index) => {
          if(minority) {
            return (
              <div key={index}>
                <MinorityCard image={minority.image}
                              name={minority.name}
                              uspercent={minority.percentage_us}
                              percentages={minority.percentages}
                              filter={this.state.searchFilter}/>
              </div>
            );
          } else {
            return;
          }
          })} searchFilter={this.state.searchFilter}
              pageCount = {this.state.pageCount}
              handlePageClick={this.handlePageClick.bind(this)}/>
      </div>
    </div>
    )
  }
}

export default Minorities;
