import React from 'react';
import { Link } from 'react-router-dom';
import { Timeline } from 'react-twitter-widgets';
import { Row, Col } from 'reactstrap';

class Person extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      data: null
    };
  }

  componentDidMount() {
    this.loadData();
  }

  loadData() {
    let minorityJSON;
    const url = "http://api.majorminorities.me/person";

    fetch(url)
      .then(res => res.json())
      .then(data => minorityJSON = data)
      .then(contents => this.setState({
        data: contents
      }))
      .catch(() => console.log("Can’t access " + url + " response. Blocked by browser?"));
  }

  render() {
    if (!this.state.data) {
      return (
        <div className="page-header" >
          <h1 style={{left:50, marginLeft:"15rem"}}> loading </h1>
        </div>
      );
    }

    var p = this.props.name;
    console.log(this.props);

    var people = this.state.data;
    var person;
    for (var x in people) {
      if (people[x].name == p)
        person = people[x];
    }
    console.log(person);
    if (!person) {
      return (
        <div className="page-header" >
          <h1 style={{left:50, marginLeft:"15rem"}}> Oops! We don't have the information you requested. </h1>
        </div>
      );
    }

    var image = person.image;
    var name = person.name;
    var dob = person.birth_date;
    var pob = person.birth_place;
    var bio = person.bio;
    var minority = person.minority_1;
    var minority2 = person.minority_2;
    var industry = person.industry;
    var occupation = person.occupation;
    var wikipedia = person.external_link;
    var youtube = "https://www.youtube.com/embed/" + person.youtube.substring(32);
    var twitter_handle = person.twitter_handle.toLowerCase();
    var website = person.website;
    console.log(website);

    return (
      <main role="main" className="container">
        <div>
          <h1 style= {{display: 'flex', justifyContent:'center', alignItems:'center', marginBottom:'40px', marginTop:'40px'}}>{name} </h1>
          <Row style={{marginBottom:'50px'}}>
            <Col style={{marginRight:'20px'}}>
              <img style={{float:'right'}} width="370px" height="255px" src={image} className="img-fluid" alt={name}></img>
            </Col>
            <Col style={{marginLeft:'20px'}}>
              <div style={{width:'400px'}}>
                <p>Date of Birth: {dob} </p>
                <p>Birthplace: {pob} </p>
                <Link to={'/minorities'}>Minority</Link>:
                <Link to={'/minority/' + minority}> {minority}</Link>
                {
                  minority2 != "None" &&
                  "/"
                }
                {
                  minority2 != "None" &&
                  <Link to={'/minority/' + minority2 }>{minority2}</Link>
                }
                <p>
                  <Link to={'/industries' }>Industry</Link>:
                  <Link to={'/industry/' + industry }> {industry}</Link>
                </p>
                <p>Occupation: {occupation} </p>
                <p>Website: <a href= {website} >{website}</a></p>
                <div>{bio}.....<a href= {wikipedia} >Learn more</a>  </div>
              </div>
            </Col>
          </Row>
          <div style={{display: 'flex', justifyContent:'center', alignItems:'center', marginBottom:'50px'}}><iframe width="78%" height="530px" src={youtube}> </iframe></div>
          <p></p>
          <p></p>
          <p></p>
          {
            twitter_handle != "none" &&
            <div style={{display: 'flex', justifyContent:'center', alignItems:'center', marginBottom:'50px'}}>
              <Timeline
                dataSource={{
                  sourceType: 'profile',
                  screenName:twitter_handle.substr(1)
                }}
                options={{
                  username: twitter_handle.substr(1),
                  width: '300px',
                  height: '500px'
                }}/>
            </div>
          }
          <div style={{height:'50px'}}> </div>
        </div>
      </main>
    )
  }
}

export default Person;
