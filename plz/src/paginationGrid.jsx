import React, { Component } from 'react';
import '@material/react-card/dist/card.css';
import ReactPaginate from 'react-paginate';

class PaginationGrid extends Component {

  handlePageClick() {
    this.props.handlePageClick();
  }

  render() {
    console.log(this.props.data);
    var cards = this.props.data;
    var cardsIntoRows = []
    var row = [];
    var x = 0;
    for (x = 0; x < 9; x++) {
      if ((x+1) % 3 == 0 && x >=2) {
        row.push(cards[x]);
        cardsIntoRows.push(row);
        row = [];
      } else {
        row.push(cards[x]);
      }
    }

    if (this.props.data.length <1) {
      return (
        <div>
          <h1 style={{display: 'flex', justifyContent:'center', alignItems:'center', marginTop: '10%'}}>
            There are no results for your search.
          </h1>
        </div>
      );
    }

    console.log(this.props);
        return (
          <div>
            <div id='peopleGrid' style={{display: 'flex', justifyContent:'center', alignItems:'center'}}>
              <Table data={cardsIntoRows} />
            </div>
            <div style={{display: 'flex', justifyContent:'center', alignItems:'center'}}>
              <ReactPaginate style={{textAlign:'middle'}}
                                    previousLabel={"previous"}
                                    nextLabel={"next"}
                                    breakLabel={<a href="">...</a>}
                                    breakClassName={"break-me"}
                                    pageCount={this.props.pageCount}
                                    forcePage={0}
                                    marginPagesDisplayed={2}
                                    pageRangeDisplayed={5}
                                    onPageChange={this.props.handlePageClick}
                                    containerClassName={"pagination"}
                                    subContainerClassName={"pages pagination"}
                                    activeClassName={"active"} />
            </div>
          </div>);
    }
};

const Cell = ({data}) => (<td>{data}</td>);

const Row = ({data}, index) => (
  <tr key={index}>
    {data.map(cell => <Cell key={data.name} data={cell} />)}
  </tr>
);

const Table = ({data}, index) => (
  <table style={{display: 'flex',  justifyContent:'center', alignItems:'center', overflowY: "auto"}}>
    <tbody> {data.map(row => <Row key={index} data={row} />)} </tbody>
  </table>
);

export default PaginationGrid;
